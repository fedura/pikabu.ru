DROP TABLE IF EXISTS tag_service;
DROP TABLE IF EXISTS tag_service_account;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS person_service_account;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS service_account;
DROP TABLE IF EXISTS service;
