SELECT *
FROM account
WHERE id > 389;

SELECT
  is_hidden,
  3,
  login,
  password
FROM account
WHERE id > 389;

-- добавить копии аккаунтов у которых айди больше 389
INSERT INTO account (is_hidden, service_id, login, password)
  SELECT
    is_hidden,
    3,
    login,
    password
  FROM account
  WHERE id > 389;

-- дать права пользователю на схему public
GRANT USAGE ON SCHEMA public TO promotion_writer;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO promotion_writer;
GRANT INSERT ON ALL TABLES IN SCHEMA public TO promotion_writer;
GRANT UPDATE ON ALL TABLES IN SCHEMA public TO promotion_writer;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO promotion_writer;

-- выбрать аккаунты у которых описание пустое но не является нуллом
SELECT *
FROM account
WHERE length(description) = 0 AND description IS NOT NULL;

-- установить полю примечание значение нулл там где это поле пустое, но не нулл
UPDATE account
SET description = NULL
WHERE length(description) = 0 AND description IS NOT NULL;

-- выбрать аккаунты с символом "земля" кроме 'ase_dag@mail.ru'
SELECT *
FROM account
WHERE id > 389 AND login LIKE '%\_%' AND login <> 'ase_dag@mail.ru';

-- удалить аккаунты с символом "земля" кроме 'ase_dag@mail.ru'
DELETE FROM account
WHERE id > 389 AND login LIKE '%\_%' AND login <> 'ase_dag@mail.ru';

SELECT *
FROM account
WHERE login = 'ninatecjb@mail.ru';

-- выбрать связку Человека и акаунта с логином 'ninatecjb@mail.ru'
SELECT *
FROM person_account
WHERE account_id = (SELECT id
                    FROM account
                    WHERE login = 'ninatecjb@mail.ru');

SELECT *
FROM person_account pa
  JOIN account a ON pa.account_id = a.id
WHERE person_id = 109;

-- удалить связку Людей и аккаунтов с заданными значениями логинов
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'killer9171',
        'kaka4a',
        'kama0590',
        'kartez2000',
        'katashe131',
        'petras'
      )
  );

-- удалить связку тегов и аккаунтов с заданными логинами
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'killer9171',
        'kaka4a',
        'kama0590',
        'kartez2000',
        'katashe131',
        'petras'
      )
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN (
  'killer9171',
  'kaka4a',
  'kama0590',
  'kartez2000',
  'katashe131',
  'petras'
);

-- удалить все связки Людей с аккаунтамив для Людей у которых нет аккаунта на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Людей у которых нет аккаунтов
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );