-- Yansonen
-- Loxxx1

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Yansonen',
            'Loxxx1'
          )
      );

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Astra55'
          )
      );