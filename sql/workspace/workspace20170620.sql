-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'kiv89',
    'killoch',
    'kargintima',
    'grinnestcusob'
  );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'kiv89',
        'killoch',
        'kargintima',
        'grinnestcusob'
      )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'kiv89',
        'killoch',
        'kargintima',
        'grinnestcusob'
      )
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN (
  'kiv89',
  'killoch',
  'kargintima',
  'grinnestcusob'
);

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

-- выбрать аккаунты без привязки к Людям
SELECT *
FROM
  account a
  LEFT JOIN person_account pa
    ON pa.account_id = a.id
WHERE
  a.is_hidden = FALSE
  AND pa.id IS NULL;

-- выбрать аккаунты Пикабу и Мэйрушечки без привязки к Людям
SELECT *
FROM
  account a
  LEFT JOIN person_account pa
    ON pa.account_id = a.id
  JOIN service s
    ON a.service_id = s.id
WHERE
  a.is_hidden = FALSE
  AND pa.id IS NULL
  AND s.code IN ('PIKABU_RU', 'MAIL_RU');

-- добавить Людей
INSERT INTO person
(code)
VALUES
  ('RussianNeuro'),
  ('musikant1'),
  ('asedag'),
  ('dialmak'),
  ('Denn29'),
  ('franzykman');

INSERT INTO person
(code)
VALUES
  ('pop2ROOTER');

-- выбрать Людей с айди больше 199
SELECT *
FROM
  person p
  JOIN account a
    ON p.code = a.login
WHERE
  p.id > 199;

-- выбрать Людей у которых айди больше 199 и код совпадает с логином какогго либо аккаунта
SELECT
  p.id,
  (SELECT acc.id
   FROM account acc
   WHERE acc.id = a.id)
FROM person p
  JOIN account a
    ON p.code = a.login
WHERE
  p.id > 199
  AND p.code = a.login;

-- связать Людей с айди больше 199 и аккаунт у кторого логин совпадает с кодом Человека
INSERT INTO person_account
(person_id, account_id)
  SELECT
    p.id,
    (SELECT acc.id
     FROM account acc
     WHERE acc.id = a.id)
  FROM person p
    JOIN account a
      ON p.code = a.login
  WHERE
    p.id > 199
    AND p.code = a.login;

-- выбрать аккаунты у которых есть стыковка с Человеком и для которых есть аккаунт с таким же точно паролем,
-- но такой аккаунт не должен быть учёткой от сервиса прокси
SELECT *
FROM
  account a_t
  JOIN account a_s
    ON a_t.id <> a_s.id
       AND a_s.password = a_t.password
       AND NOT EXISTS(
      SELECT NULL
      FROM service s
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE
        s.id = a_t.service_id
        AND t.code = 'PROXY'
  )
  JOIN person_account pa
    ON a_s.id = pa.account_id;

-- выбрать логины аккаунтов у которых совпадают пароли и есть привязка к Человеку
SELECT
  a_t.login AS target,
  a_s.login AS source
FROM
  account a_t
  JOIN account a_s
    ON a_t.id <> a_s.id
       AND a_s.password = a_t.password
       AND NOT EXISTS(
      SELECT NULL
      FROM service s
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE
        s.id = a_t.service_id
        AND t.code = 'PROXY'
  )
  JOIN person_account pa
    ON a_s.id = pa.account_id;

-- выбрать айди аккаунтов у которых пароли совпадают и есть привязка к Человеку
SELECT
  a_t.id,
  a_s.id
FROM
  account a_t
  JOIN account a_s
    ON a_t.id <> a_s.id
       AND a_s.password = a_t.password
       AND NOT EXISTS(
      SELECT NULL
      FROM service s
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE
        s.id = a_t.service_id
        AND t.code = 'PROXY'
  )
  JOIN person_account pa
    ON a_s.id = pa.account_id;

-- выбрать логиин аккаунта с которым совпал пароль и выбрать код связанного Человека
SELECT
  a_t.login,
  (SELECT code
   FROM person p
   WHERE p.id = pa.person_id)
FROM
  account a_t
  JOIN account a_s
    ON a_t.id <> a_s.id
       AND a_s.password = a_t.password
       AND NOT EXISTS(
      SELECT NULL
      FROM service s
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE
        s.id = a_t.service_id
        AND t.code = 'PROXY'
  )
  JOIN person_account pa
    ON a_s.id = pa.account_id;

SELECT
  a_t.id,
  pa.person_id
FROM
  account a_t
  JOIN account a_s
    ON a_t.id <> a_s.id
       AND a_s.password = a_t.password
       AND NOT EXISTS(
      SELECT NULL
      FROM service s
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE
        s.id = a_t.service_id
        AND t.code = 'PROXY'
  )
  JOIN person_account pa
    ON a_s.id = pa.account_id;

-- вставить связку Человека и аккаунта с которым совпадает пароль
INSERT INTO person_account
(person_id, account_id)
  SELECT
    pa.person_id,
    a_t.id
  FROM
    account a_t
    JOIN account a_s
      ON a_t.id <> a_s.id
         AND a_s.password = a_t.password
         AND NOT EXISTS(
        SELECT NULL
        FROM service s
          JOIN tag_service ts
            ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE
          s.id = a_t.service_id
          AND t.code = 'PROXY'
    )
    JOIN person_account pa
      ON a_s.id = pa.account_id;
;

-- выбрать сервисы у которых код похож на заданные строки
SELECT *
FROM
  service s
WHERE
  s.code LIKE '94.26.192.11:24531%'
  OR s.code LIKE '185.139.213.140:24531%'
  OR s.code LIKE '185.139.213.203%';

-- установи связку Человека с кодом 'pop2ROOTER' и аккаунта от сервиса с кодом похожим на '94.26.192.11:24531%'
INSERT INTO person_account
(person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'pop2ROOTER'),
    (SELECT a.id
     FROM service s
       JOIN account a ON s.id = a.service_id
     WHERE code LIKE '94.26.192.11:24531%');

INSERT INTO person_account
(person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'RussianNeuro'),
    (SELECT a.id
     FROM service s
       JOIN account a ON s.id = a.service_id
     WHERE code LIKE '185.139.213.140:24531%');

INSERT INTO person_account
(person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'musikant1'),
    (SELECT a.id
     FROM service s
       JOIN account a ON s.id = a.service_id
     WHERE code LIKE '185.139.213.203%');

SELECT
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
    WHERE
      ps.id = p.id
      AND s.code = 'MAIL_RU'
  ) AS login_mail_ru,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
    WHERE
      ps.id = p.id
      AND s.code = 'MAIL_RU'
  ) AS password_mail_ru,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
    WHERE
      ps.id = p.id
      AND s.code = 'PIKABU_RU'
  ) AS login_pikabu_ru,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
    WHERE
      ps.id = p.id
      AND s.code = 'PIKABU_RU'
  ) AS password_pikabu_ru,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      ps.id = p.id
      AND t.code = 'PROXY'
  ) AS login_proxy,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      ps.id = p.id
      AND t.code = 'PROXY'
  ) AS password_proxy,
  (
    SELECT count(*)
    FROM person ps
      JOIN person_account pa
        ON ps.id = pa.person_id
      JOIN account a
        ON pa.account_id = a.id
      JOIN service s ON
                       a.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      ps.id = p.id
      AND t.code = 'PROXY'
  ) AS address_proxy,
  p.code

FROM
  person p
ORDER BY p.id;

SELECT *
FROM person ps
  JOIN person_account pa
    ON ps.id = pa.person_id
  JOIN account a
    ON pa.account_id = a.id
  JOIN service s ON
                   a.service_id = s.id
WHERE
  ps.code IN ('musikant1', 'pop2ROOTER')
  AND s.code = 'MAIL_RU';

DELETE FROM person_account
WHERE id IN (683, 685);

SELECT *
FROM person_account
WHERE account_id = 76;

DELETE FROM person_account
WHERE id IN (688);

SELECT *
FROM person_account
WHERE account_id = 195;

DELETE FROM person_account
WHERE id IN (690);

SELECT *
FROM person_account
WHERE person_id IS NULL;

-- выбрать тег 'RELIABLE' и заданные аккаунты
SELECT
  (SELECT code
   FROM tag
   WHERE code = 'RELIABLE'),
  a.login
FROM
  account a
WHERE
  a.login IN
  (
    'RussianNeuro',
    'musikant1',
    'asedag',
    'dialmak',
    'Denn29',
    'franzykman',
    'pop2ROOTER'
  );

-- выбрать айди для тега 'RELIABLE' и айди для заданных аккаунтов
SELECT
  (SELECT id
   FROM tag
   WHERE code = 'RELIABLE'),
  a.id
FROM
  account a
WHERE
  a.login IN
  (
    'RussianNeuro',
    'musikant1',
    'asedag',
    'dialmak',
    'Denn29',
    'franzykman',
    'pop2ROOTER'
  );

-- проставить тег 'RELIABLE' для заданных аккаунтов
INSERT INTO tag_account (tag_id, account_id)
  SELECT
    (SELECT id
     FROM tag
     WHERE code = 'RELIABLE'),
    a.id
  FROM
    account a
  WHERE
    a.login IN
    (
      'RussianNeuro',
      'musikant1',
      'asedag',
      'dialmak',
      'Denn29',
      'franzykman',
      'pop2ROOTER'
    );

-- выбрать не привязанные проксики и заданные аккаунты у которых для Человека не назначен прокси
WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_account psa
                        WHERE
                          psa.account_id = sa.id
                    ))
)
SELECT
  (
    SELECT s.code
    FROM
      account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_account psa
          WHERE
            psa.account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT (select code from person where id = psa.person_id)
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL
;

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_account psa
                        WHERE
                          psa.account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_account psa
          WHERE
            psa.account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL
;

INSERT INTO person_account (account_id, person_id)
    WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_account psa
                        WHERE
                          psa.account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_account psa
          WHERE
            psa.account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND sa.login IN ('asedag', 'dialmak', 'Denn29', 'franzykman')
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL
;

-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'etepfici',
    'koly4ka001',
    'britvas',
    'kamazzz21',
    'kat3959',
    'kisik3',
    'kimenhack',
    'kblc9l',
    'kirklim',
    'aleksandrkurn',
    'kaban7610'
  );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
    'etepfici',
    'koly4ka001',
    'britvas',
    'kamazzz21',
    'kat3959',
    'kisik3',
    'kimenhack',
    'kblc9l',
    'kirklim',
    'aleksandrkurn',
    'kaban7610'
      )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
    'etepfici',
    'koly4ka001',
    'britvas',
    'kamazzz21',
    'kat3959',
    'kisik3',
    'kimenhack',
    'kblc9l',
    'kirklim',
    'aleksandrkurn',
    'kaban7610'
      )
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN (
    'etepfici',
    'koly4ka001',
    'britvas',
    'kamazzz21',
    'kat3959',
    'kisik3',
    'kimenhack',
    'kblc9l',
    'kirklim',
    'aleksandrkurn',
    'kaban7610'
);

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

-- очистить связку аккаунтов и постов
TRUNCATE TABLE account_post;