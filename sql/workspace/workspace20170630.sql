SELECT *
FROM
  person p
  JOIN person_account pa
    ON p.id = pa.person_id
  JOIN account a
    ON pa.account_id = a.id
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN tag_account ta
    ON a.id = ta.account_id
  LEFT JOIN tag tta
    ON ta.tag_id = tta.id
  LEFT JOIN tag_service ts
    ON s.id = ts.service_id
  LEFT JOIN tag tts
    ON ts.tag_id = tts.id;

SELECT
  to_char(row_number()
          OVER (), '09') ||
  ' http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- account tag
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  )
    AS person_account,
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  ) AS account_tag
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM
        account a
        JOIN tag_account ta
          ON a.id = ta.account_id
        JOIN tag t
          ON ta.tag_id = t.id
        JOIN person_account pa
          ON a.id = pa.account_id
        JOIN person pe
          ON pa.person_id = pe.id
      WHERE
        pe.id = p.id
        AND t.code = 'RELIABLE'
        AND a.service_id =
            (
              SELECT id
              FROM service
              WHERE code = 'PIKABU_RU'
            )
        AND a.is_hidden = FALSE
  )
ORDER BY account_tag DESC;

-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'WatsonRus'
  )
  AND a.service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'WatsonRus'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'WatsonRus'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить аккаунты
DELETE FROM account
WHERE
  login IN
  (
    'WatsonRus'
  )
  AND service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

-- Denhead
SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Denhead%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'Denhead',
    'denhead@list.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '31.184.235.83:24531%';

INSERT INTO person
(code) VALUES ('Denhead');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT id
    FROM account
    WHERE login = 'Denhead')),
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT id
    FROM account
    WHERE login = 'denhead@list.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '31.184.235.83:24531%'));

SELECT
  p.id           AS post_id,
  btrim(p.title) AS post_title,
  p.body         AS post_body,
  p.bulk_tags    AS post_tags
FROM
  post p
  LEFT JOIN account_post ap
    ON p.id = ap.post_id
WHERE
  p.is_hidden IS TRUE
  AND ap.id IS NULL;

SELECT
  account_id,
  (SELECT login
   FROM account a
   WHERE a.id = ap.account_id),
  post_id,
  (SELECT title
   FROM post p
   WHERE p.id = ap.post_id)
FROM
  account_post ap
WHERE
  ap.insert_date > '2017-06-30';

DELETE FROM account_post
WHERE id IN (119, 127);

--
SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Denhead%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'addhaloka',
    'addhaloka@mail.ru',
    'antipod66@bk.ru',
    'Antipod66',
    'playyob@bk.ru',
    'PLAYYOB',
    'alexicus@bk.ru',
    'alexicus'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND
  (
    s.code LIKE '185.139.213.132:24531%'
    OR s.code LIKE '146.185.249.12:24531%'
    OR s.code LIKE '185.47.207.83:24531%'
    OR s.code LIKE '31.184.233.128:24531%'
  );

INSERT INTO person
(code) VALUES ('addhaloka'), ('Antipod66'), ('PLAYYOB'), ('alexicus');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'addhaloka'),
   (SELECT id
    FROM account
    WHERE login = 'addhaloka')),
  ((SELECT id
    FROM person
    WHERE code = 'addhaloka'),
   (SELECT id
    FROM account
    WHERE login = 'addhaloka@mail.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'addhaloka'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '31.184.233.128:24531%')),

  ((SELECT id
    FROM person
    WHERE code = 'Antipod66'),
   (SELECT id
    FROM account
    WHERE login = 'Antipod66')),
  ((SELECT id
    FROM person
    WHERE code = 'Antipod66'),
   (SELECT id
    FROM account
    WHERE login = 'antipod66@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Antipod66'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '185.47.207.83:24531%')),

  ((SELECT id
    FROM person
    WHERE code = 'PLAYYOB'),
   (SELECT id
    FROM account
    WHERE login = 'PLAYYOB')),
  ((SELECT id
    FROM person
    WHERE code = 'PLAYYOB'),
   (SELECT id
    FROM account
    WHERE login = 'playyob@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'PLAYYOB'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '146.185.249.12:24531%')),

  ((SELECT id
    FROM person
    WHERE code = 'alexicus'),
   (SELECT id
    FROM account
    WHERE login = 'alexicus')),
  ((SELECT id
    FROM person
    WHERE code = 'alexicus'),
   (SELECT id
    FROM account
    WHERE login = 'alexicus@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'alexicus'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '185.139.213.132:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'alexicus',
        'PLAYYOB',
        'Antipod66',
        'addhaloka'
      )
)
SELECT
  acc.account_id,
  t.id
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'alexicus',
          'PLAYYOB',
          'Antipod66',
          'addhaloka'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );

SELECT *
FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'alexicus',
            'PLAYYOB',
            'addhaloka'
          )
      );

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'alexicus',
            'PLAYYOB',
            'addhaloka'
          )
      );

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'alexicus',
        'PLAYYOB',
        'addhaloka'
      )
)
SELECT
  acc.account_id,
  t.id
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'BLOCK'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'alexicus',
          'PLAYYOB',
          'addhaloka'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'BLOCK'
    );

SELECT *
FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'pop2ROOTER'
          )
      );

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'pop2ROOTER'
          )
      );

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'pop2ROOTER'
      )
)
SELECT
  acc.account_id,
  t.id
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'BLOCK'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'pop2ROOTER'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'BLOCK'
    );

/*
winddustselfle
Denn29
Denhead
Antipod66
dialmak
franzykman

*/

SELECT *
FROM account
WHERE
  login IN
  (
    'winddustselfle',
    'Denn29',
    'franzykman'
  );

UPDATE account
SET is_hidden = TRUE
WHERE login IN
      (
        'winddustselfle',
        'Denn29',
        'franzykman',
        'Denhead'
      )
;

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'winddustselfle',
        'Denn29',
        'franzykman',
        'Denhead'
      )
;