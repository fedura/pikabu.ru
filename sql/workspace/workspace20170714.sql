-- Denn29
-- Antipod66
-- KapralBel
-- AntiMember

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Denn29',
            'Antipod66',
            'KapralBel',
            'AntiMember'
          )
      );