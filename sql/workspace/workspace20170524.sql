SELECT COUNT(*)
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id
WHERE
  t.code = 'PROXY'
  AND NOT EXISTS
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT (SELECT sa.login
        FROM service_account sa
          JOIN tag_service_account tsa
            ON sa.id = tsa.service_account_id
          JOIN tag t
            ON tsa.tag_id = t.id
        WHERE sa.service_id = (SELECT id
                               FROM service
                               WHERE code = 'PIKABU_RU')
              AND t.code = 'RELIABLE'
              AND NOT EXISTS(SELECT NULL
                             FROM person_service_account psa
                             WHERE psa.service_account_id = sa.id)
        OFFSET r.i
        LIMIT 1) AS account_login
FROM
  r
WHERE
  (SELECT sa.login
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS(SELECT NULL
                        FROM person_service_account psa
                        WHERE psa.service_account_id = sa.id)
   OFFSET r.i
   LIMIT 1)
  IS NOT NULL;

INSERT INTO person (code)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT (SELECT sa.login
          FROM service_account sa
            JOIN tag_service_account tsa
              ON sa.id = tsa.service_account_id
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE sa.service_id = (SELECT id
                                 FROM service
                                 WHERE code = 'PIKABU_RU')
                AND t.code = 'RELIABLE'
                AND NOT EXISTS(SELECT NULL
                               FROM person_service_account psa
                               WHERE psa.service_account_id = sa.id)
          OFFSET r.i
          LIMIT 1) AS account_login
  FROM
    r
  WHERE
    (SELECT sa.login
     FROM service_account sa
       JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id
       JOIN tag t
         ON tsa.tag_id = t.id
     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND t.code = 'RELIABLE'
           AND NOT EXISTS(SELECT NULL
                          FROM person_service_account psa
                          WHERE psa.service_account_id = sa.id)
     OFFSET r.i
     LIMIT 1)
    IS NOT NULL;

SELECT
  p.id,
  (
    SELECT sa.id
    FROM service_account sa
    WHERE sa.login = p.code
  )
FROM
  person p
WHERE
  NOT EXISTS(
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.person_id = p.id
  );

INSERT INTO person_service_account (person_id, service_account_id)
  SELECT
    p.id,
    (
      SELECT sa.id
      FROM service_account sa
      WHERE sa.login = p.code
    )
  FROM
    person p
  WHERE
    NOT EXISTS(
        SELECT NULL
        FROM
          person_service_account psa
        WHERE
          psa.person_id = p.id
    );


WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      service_account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_service_account psa
          WHERE
            psa.service_account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.person_id
    FROM
      person_service_account psa
      JOIN service_account sa
        ON psa.service_account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_service_account tsa
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE
            tsa.service_account_id = sa.id
            AND t.code = 'FAIL'
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_service_account psae
              ON pe.id = psae.person_id
            JOIN service_account sae
              ON psae.service_account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_service_account psa
      JOIN service_account sa
        ON psa.service_account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_service_account tsa
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE
            tsa.service_account_id = sa.id
            AND t.code = 'FAIL'
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_service_account psae
              ON pe.id = psae.person_id
            JOIN service_account sae
              ON psae.service_account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL;

INSERT INTO person_service_account (service_account_id, person_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (
      SELECT sa.id
      FROM
        service_account sa
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        t.code = 'PROXY'
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_service_account psa
            WHERE
              psa.service_account_id = sa.id
        )
      OFFSET r.i
      LIMIT 1) AS proxy_account,
    (
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) AS person
  FROM
    r
  WHERE
    (
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) IS NOT NULL;

SELECT 'http://'
       || -- proxy_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ':'
       || -- proxy_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || '@'
       || -- proxy_address
       (SELECT s.code
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  AS proxy_login_password
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
ORDER BY p.id;

SELECT *
FROM person p
  JOIN person_service_account psa ON p.id = psa.person_id
  JOIN service_account sa ON psa.service_account_id = sa.id
  JOIN service s ON sa.service_id = s.id
  LEFT JOIN tag_service ts ON s.id = ts.service_id
  LEFT JOIN tag tts ON ts.tag_id = tts.id
  LEFT JOIN tag_service_account tsa ON sa.id = tsa.service_account_id
  LEFT JOIN tag tsat ON tsa.tag_id = tsat.id;

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT (SELECT sa.login
        FROM service_account sa
          LEFT JOIN tag_service_account tsa
            ON sa.id = tsa.service_account_id
          LEFT JOIN tag t
            ON tsa.tag_id = t.id
        WHERE sa.service_id = (SELECT id
                               FROM service
                               WHERE code = 'PIKABU_RU')
              AND (t.code <> 'RELIABLE' OR t.code IS NULL)
              AND NOT EXISTS(SELECT NULL
                             FROM person_service_account psa
                             WHERE psa.service_account_id = sa.id)
        OFFSET r.i
        LIMIT 1) AS account_login
FROM
  r
WHERE
  (SELECT sa.login
   FROM service_account sa
     LEFT JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     LEFT JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND (t.code <> 'RELIABLE' OR t.code IS NULL)
         AND NOT EXISTS(SELECT NULL
                        FROM person_service_account psa
                        WHERE psa.service_account_id = sa.id)
   OFFSET r.i
   LIMIT 1)
  IS NOT NULL;

INSERT INTO person (code)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT (SELECT sa.login
          FROM service_account sa
            LEFT JOIN tag_service_account tsa
              ON sa.id = tsa.service_account_id
            LEFT JOIN tag t
              ON tsa.tag_id = t.id
          WHERE sa.service_id = (SELECT id
                                 FROM service
                                 WHERE code = 'PIKABU_RU')
                AND (t.code <> 'RELIABLE' OR t.code IS NULL)
                AND NOT EXISTS(SELECT NULL
                               FROM person_service_account psa
                               WHERE psa.service_account_id = sa.id)
          OFFSET r.i
          LIMIT 1) AS account_login
  FROM
    r
  WHERE
    (SELECT sa.login
     FROM service_account sa
       LEFT JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id
       LEFT JOIN tag t
         ON tsa.tag_id = t.id
     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND (t.code <> 'RELIABLE' OR t.code IS NULL)
           AND NOT EXISTS(SELECT NULL
                          FROM person_service_account psa
                          WHERE psa.service_account_id = sa.id)
     OFFSET r.i
     LIMIT 1)
    IS NOT NULL;

SELECT
  p.id,
  (
    SELECT sa.id
    FROM service_account sa
    WHERE sa.login = p.code
  )
FROM
  person p
WHERE
  NOT EXISTS(
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.person_id = p.id
  );

INSERT INTO person_service_account (person_id, service_account_id)
  SELECT
    p.id,
    (
      SELECT sa.id
      FROM service_account sa
      WHERE sa.login = p.code
    )
  FROM
    person p
  WHERE
    NOT EXISTS(
        SELECT NULL
        FROM
          person_service_account psa
        WHERE
          psa.person_id = p.id
    );


  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (
      SELECT sa.id
      FROM
        service_account sa
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        t.code = 'PROXY'
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_service_account psa
            WHERE
              psa.service_account_id = sa.id
        )
      OFFSET r.i
      LIMIT 1) AS proxy_account,
    (
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) AS person
  FROM
    r
WHERE
(
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) IS NOT NULL;

INSERT INTO person_service_account (service_account_id, person_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (
      SELECT sa.id
      FROM
        service_account sa
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        t.code = 'PROXY'
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_service_account psa
            WHERE
              psa.service_account_id = sa.id
        )
      OFFSET r.i
      LIMIT 1) AS proxy_account,
    (
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) AS person
  FROM
    r
WHERE
(
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) IS NOT NULL;

SELECT 'http://'
       || -- proxy_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ':'
       || -- proxy_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || '@'
       || -- proxy_address
       (SELECT s.code
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  AS proxy_login_password
FROM
  person p
WHERE
  EXISTS(
    SELECT NULL FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY'
  )
ORDER BY proxy_login_password;
