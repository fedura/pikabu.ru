-- посчитать не занятые проксики
SELECT COUNT(*)
FROM
  account sa
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id
WHERE
  t.code = 'PROXY'
  AND NOT EXISTS
  (
      SELECT NULL
      FROM
        person_account psa
      WHERE
        psa.account_id = sa.id
  );

-- для не занятых прокси вывести людей у которых есть Пикабу но нет Прокси,
-- количество строк ограничить
-- количеством свободных прокси
-- количеством людей у которых есть Пикабу и нет Прокси
WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_account psa
                        WHERE
                          psa.account_id = sa.id
                    ))
)
SELECT
  (
    SELECT s.code
    FROM
      account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_account psa
          WHERE
            psa.account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT (SELECT code
            FROM person
            WHERE id = psa.person_id)
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_account tsa
          WHERE
            tsa.account_id = sa.id
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_account tsa
          WHERE
            tsa.account_id = sa.id
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL;

-- вывести айди от Людей с Пикабу которым нужен прокси и айди не занятых Прокси
WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_account psa
                        WHERE
                          psa.account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_account psa
          WHERE
            psa.account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_account tsa
          WHERE
            tsa.account_id = sa.id
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r
WHERE
  (
    SELECT psa.person_id
    FROM
      person_account psa
      JOIN account sa
        ON psa.account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_account tsa
          WHERE
            tsa.account_id = sa.id
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_account psae
              ON pe.id = psae.person_id
            JOIN account sae
              ON psae.account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) IS NOT NULL;

-- Назначить Прокси Людям с Пикабу
INSERT INTO person_account (account_id, person_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_account psa
                          WHERE
                            psa.account_id = sa.id
                      ))
  )
  SELECT
    (
      SELECT sa.id
      FROM
        account sa
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        t.code = 'PROXY'
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_account psa
            WHERE
              psa.account_id = sa.id
        )
      OFFSET r.i
      LIMIT 1) AS proxy_account,
    (
      SELECT psa.person_id
      FROM
        person_account psa
        JOIN account sa
          ON psa.account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_account tsa
            WHERE
              tsa.account_id = sa.id
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_account psae
                ON pe.id = psae.person_id
              JOIN account sae
                ON psae.account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) AS person
  FROM
    r
  WHERE
    (
      SELECT psa.person_id
      FROM
        person_account psa
        JOIN account sa
          ON psa.account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_account tsa
            WHERE
              tsa.account_id = sa.id
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_account psae
                ON pe.id = psae.person_id
              JOIN account sae
                ON psae.account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) IS NOT NULL;

-- аккаунты всех Людей
SELECT *
FROM person p
  JOIN person_account psa ON p.id = psa.person_id
  JOIN account sa ON psa.account_id = sa.id
  JOIN service s ON sa.service_id = s.id
  LEFT JOIN tag_service ts ON s.id = ts.service_id
  LEFT JOIN tag tts ON ts.tag_id = tts.id
  LEFT JOIN tag_account tsa ON sa.id = tsa.account_id
  LEFT JOIN tag tsat ON tsa.tag_id = tsat.id;

-- сформировать запись Профиля ЗенноПостера
SELECT to_char(row_number()
               OVER (), '99')
       || ' http://'
       || -- proxy_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ':'
       || -- proxy_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || '@'
       || -- proxy_address
       (SELECT s.code
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'MAIL_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  AS person_accounts
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND NOT EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN tag_account ts ON sa.id = ts.account_id
      WHERE ps.id = p.id
  )
ORDER BY person_accounts;

-- показать аккаунты с заднными логинами
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  )
  AND a.service_id =
      (
        SELECT id
        FROM service
        WHERE code = 'PIKABU_RU'
      );

-- удалить связку между тегами и заданными аккаунтами
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'mikhailfil',
        'stadevlasov',
        'aleshazhu',
        'ilyasti',
        'petrlukin',
        'petrevya',
        'fedorwm',
        'petrzimin',
        'nikitos66',
        'leonidust',
        'mikhail1un',
        'nikitada',
        'anatoliytim',
        'igorturov',
        'nikolaylow',
        'petrana',
        'mikhailempopo'
      )
      AND a.service_id =
          (
            SELECT id
            FROM service
            WHERE code = 'PIKABU_RU'
          )
  );

-- удалить связку Человека и заданных аккаунтов
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'mikhailfil',
        'stadevlasov',
        'aleshazhu',
        'ilyasti',
        'petrlukin',
        'petrevya',
        'fedorwm',
        'petrzimin',
        'nikitos66',
        'leonidust',
        'mikhail1un',
        'nikitada',
        'anatoliytim',
        'igorturov',
        'nikolaylow',
        'petrana',
        'mikhailempopo'
      )
      AND a.service_id =
          (
            SELECT id
            FROM service
            WHERE code = 'PIKABU_RU'
          )
  );

-- удалить заданные аккаунты
DELETE FROM
  account a
WHERE
  a.login IN
  (
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  )
  AND a.service_id =
      (
        SELECT id
        FROM service
        WHERE code = 'PIKABU_RU'
      );

-- добавить аккаунты Пикабу
INSERT INTO account (login, password, service_id)
VALUES
  ('mikhailfil', 'h3cV9524', 3),
  ('stadevlasov', 'Vk8W8C0o', 3),
  ('aleshazhu', 'u1eHn77q25', 3),
  ('ilyasti', 'nNu9PcclTi', 3),
  ('petrlukin', 'knIo77Nwz', 3),
  ('petrevya', 'bTHBz79UiB', 3),
  ('fedorwm', 'LtnacSb9ZO', 3),
  ('petrzimin', 'N39679m5', 3),
  ('nikitos66', 'uijkHLbJe66', 3),
  ('leonidust', 'LIBkB6f8Ba	', 3),
  ('mikhail1un', 'x1ofViD8um', 3),
  ('nikitada', 'jY8Os1o2yv', 3),
  ('anatoliytim', 'i1xMlZoe', 3),
  ('igorturov', 'bk7wz5Y1', 3),
  ('nikolaylow', 'ssA4gr5j', 3),
  ('petrana', 'v7J23jguf', 3),
  ('mikhailempopo', 'I2a1ehhNy', 3);

-- выбрать айди Людей и Аккаунтов у которых совпадают код и логин и между которыми не установлена связь
SELECT
  p.id,
  (SELECT id
   FROM
     account
   WHERE
     login = p.code)
FROM person p
WHERE EXISTS
      (
          SELECT NULL
          FROM account a
          WHERE p.code = a.login
      )
      AND NOT EXISTS
(
    SELECT NULL
    FROM
      person_account pa
    WHERE
      pa.account_id = (SELECT id
                       FROM account
                       WHERE login = p.code)
);

-- установит ьсвязь между Людьми и Аккаунтами по совпадению кода и логина
INSERT INTO person_account (person_id, account_id)
  SELECT
    p.id,
    (SELECT id
     FROM
       account
     WHERE
       login = p.code)
  FROM person p
  WHERE EXISTS
        (
            SELECT NULL
            FROM account a
            WHERE p.code = a.login
        )
        AND NOT EXISTS
  (
      SELECT NULL
      FROM
        person_account pa
      WHERE
        pa.account_id = (SELECT id
                         FROM account
                         WHERE login = p.code)
  );

-- выбрать тег с кодом 'BLOCK' и айди заданных аккаунтов
SELECT
  (select code from tag where code = 'BLOCK'),
  id
FROM
  account
WHERE
  login in
  (
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  )
;

-- установить тег 'BLOCK' для заданных аккаунтов
INSERT INTO tag_account (tag_id, account_id)
    SELECT
  (select id from tag where code = 'BLOCK'),
  id
FROM
  account
WHERE
  login in
  (
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  )
;
