SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Denchik%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'mrDenchik007',
    'mister.denchik007@mail.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '31.184.235.99:24531%';

INSERT INTO person
(code) VALUES ('mrDenchik007');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'mrDenchik007'),
   (SELECT id
    FROM account
    WHERE login = 'mrDenchik007')),
  ((SELECT id
    FROM person
    WHERE code = 'mrDenchik007'),
   (SELECT id
    FROM account
    WHERE login = 'mister.denchik007@mail.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'mrDenchik007'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '31.184.235.99:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'mrDenchik007'
      )
)
SELECT
  acc.account_id,
  (SELECT login
   FROM account
   WHERE id = acc.account_id),
  t.id,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'mrDenchik007'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );

SELECT
  ltrim(to_char(row_number()
                OVER (), '09')) ||
  ' http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
    AS person_account,
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  ) AS account_tag
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM
        account a
        JOIN tag_account ta
          ON a.id = ta.account_id
        JOIN tag t
          ON ta.tag_id = t.id
        JOIN person_account pa
          ON a.id = pa.account_id
        JOIN person pe
          ON pa.person_id = pe.id
      WHERE
        pe.id = p.id
        AND t.code = 'RELIABLE'
        AND a.service_id =
            (
              SELECT id
              FROM service
              WHERE code = 'PIKABU_RU'
            )
        AND a.is_hidden = FALSE
  )
ORDER BY account_tag DESC;

SELECT *
FROM person
WHERE code = 'mrDenchik007';

SELECT *
FROM
  person p

WHERE p.code = 'mrDenchik007';

SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Loxxx%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'Loxxx1',
    'loxxx2007@bk.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '185.47.205.248:24531%';

INSERT INTO person
(code) VALUES ('Loxxx1');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'Loxxx1'),
   (SELECT id
    FROM account
    WHERE login = 'Loxxx1')),
  ((SELECT id
    FROM person
    WHERE code = 'Loxxx1'),
   (SELECT id
    FROM account
    WHERE login = 'loxxx2007@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Loxxx1'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '185.47.205.248:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'Loxxx1'
      )
)
SELECT
  acc.account_id,
  (SELECT login
   FROM account
   WHERE id = acc.account_id),
  t.id,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'Loxxx1'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );