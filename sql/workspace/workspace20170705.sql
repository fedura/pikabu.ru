SELECT
  ltrim(to_char(row_number()
                OVER (), '09')) ||
  ' http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- account tag
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  )
    AS person_account,
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  ) AS account_tag
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM
        account a
        JOIN tag_account ta
          ON a.id = ta.account_id
        JOIN tag t
          ON ta.tag_id = t.id
        JOIN person_account pa
          ON a.id = pa.account_id
        JOIN person pe
          ON pa.person_id = pe.id
      WHERE
        pe.id = p.id
        AND t.code = 'RELIABLE'
        AND a.service_id =
            (
              SELECT id
              FROM service
              WHERE code = 'PIKABU_RU'
            )
        AND a.is_hidden = FALSE
  )
ORDER BY account_tag DESC;

SELECT *
FROM
  tag_account ta
  JOIN account a
    ON ta.account_id = a.id
  JOIN tag t
    ON ta.tag_id = t.id
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Serjik2'
          )
      );

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Serjik2'
          )
      );

SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Chauvinist%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'Chauvinist',
    'chauvinist@bk.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '31.184.233.14:24531%';

INSERT INTO person
(code) VALUES ('Chauvinist');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'Chauvinist'),
   (SELECT id
    FROM account
    WHERE login = 'Chauvinist')),
  ((SELECT id
    FROM person
    WHERE code = 'Chauvinist'),
   (SELECT id
    FROM account
    WHERE login = 'chauvinist@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Chauvinist'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '31.184.233.14:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'Chauvinist'
      )
)
SELECT
  acc.account_id,
  (SELECT login
   FROM account
   WHERE id = acc.account_id),
  t.id,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'Chauvinist'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );

/*
Trava79 Отправляем пост
franzykman запостили
Denn29 Загадка про что-то БАЯН
dialmak По жене соскучился БАЯН
Astra55 запостили
Antipod66 Говорит учитель БАЯН

*/
UPDATE account
SET is_hidden = TRUE
WHERE login IN
      (
        'Trava79',
        'franzykman',
        'Astra55',
        'Denn29',
        'dialmak',
        'Chauvinist'
      );

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'Trava79',
        'franzykman',
        'Astra55',
        'Denn29',
        'dialmak',
        'Chauvinist'
      );

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'winddustselfle'
      );

SELECT *
FROM
  tag_account ta
  JOIN account a
    ON ta.account_id = a.id
  JOIN tag t
    ON ta.tag_id = t.id
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'winddustselfle'
          )
      );

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'winddustselfle'
          )
      );

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'Antipod66',
        'abipsdatun'
      );

UPDATE account
SET is_hidden = TRUE
WHERE login IN
      (
        'dialmak',
        'Denn29'
      );

UPDATE account
SET is_hidden = TRUE
WHERE login IN
      (
        'Antipod66',
        'dialmak',
        'Trava79'
      );

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'Antipod66',
        'dialmak',
        'Trava79'
      );

/*
MAIL_RU NPC Zc2LJbKPtWwJ7KSnEVcP
94.26.192.32:24531 kbhnbx RFnDztb35K
05-07-1989 женский
Ирина Малышкова
npc0001@mail.ru
NPC0001
*/

SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%NPC0001%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'NPC0001',
    'npc0001@mail.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '94.26.192.32:24531%';

INSERT INTO person
(code) VALUES ('NPC0001');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'NPC0001'),
   (SELECT id
    FROM account
    WHERE login = 'NPC0001')),
  ((SELECT id
    FROM person
    WHERE code = 'NPC0001'),
   (SELECT id
    FROM account
    WHERE login = 'npc0001@mail.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'NPC0001'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '94.26.192.32:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'NPC0001'
      )
)
SELECT
  acc.account_id,
  (SELECT login
   FROM account
   WHERE id = acc.account_id),
  t.id,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'NPC0001'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );

/*
Denn29
Antipod66
NPC0001
Astra55
dialmak
Chauvinist
Trava79
franzykman
*/
UPDATE account
SET is_hidden = TRUE
WHERE login IN
      (
        'Denn29',
        'Antipod66',
        'Astra55',
        'dialmak',
        'Chauvinist',
        'Trava79',
        'franzykman'
      );

UPDATE account
SET is_hidden = FALSE
WHERE login IN
      (
        'Denn29',
        'Antipod66',
        'Astra55',
        'dialmak',
        'Chauvinist',
        'Trava79',
        'franzykman'
      );