-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'kipriano',
    'kid007',
    'karkyshka',
    'kennympei',
    'mikhaildb',
    'kl9n'
  );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'kipriano',
        'kid007',
        'karkyshka',
        'kennympei',
        'mikhaildb',
        'kl9n'
      )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'kipriano',
        'kid007',
        'karkyshka',
        'kennympei',
        'mikhaildb',
        'kl9n'
      )
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN (
  'kipriano',
  'kid007',
  'karkyshka',
  'kennympei',
  'mikhaildb',
  'kl9n'
);

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'kapamel1k',
    'tiobalabest',
    'kohgutep'
  );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'kapamel1k',
        'tiobalabest',
        'kohgutep'
      )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'kapamel1k',
        'tiobalabest',
        'kohgutep'
      )
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN (
  'kapamel1k',
  'tiobalabest',
  'kohgutep'
);

-- выбрать из указанных акков только те кто имеет тег Существенный
SELECT *
FROM
  tag_account ta
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'dresuncuche',
        'staphagtiostooz',
        'sleekefclanfeu',
        'inanexdean',
        'tiborguitwor',
        'vijnofebdo',
        'dingvalrudep',
        'necnawhire',
        'katy5452',
        'choisosale',
        'destnoumerly',
        'tansurpsendcam',
        'heartclevnica',
        'mercarogar',
        'disfdeberpo',
        'kamicfeici',
        'glutaqdymil',
        'terticora',
        'zithrinidi',
        'gausithirshu',
        'mantnacina',
        'battryvorma',
        'fighheacentri',
        'brousoutmino',
        'swabmicquosu',
        'encatitu',
        'imphocogre',
        'ivenganes',
        'ceiceerekul',
        'highcommoucha',
        'inhenfeverb',
        'lapneucewo',
        'worvolksanca',
        'lecnaribna',
        'geocintingprin',
        'belihinknusc',
        'practerrero',
        'olanomvi',
        'boabekimoun',
        'ibatelzu1984'
      )
      AND tag_id = (SELECT id
                    FROM tag
                    WHERE code = 'RELIABLE')
  );

DELETE FROM tag_account ta
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account sa
    WHERE
      sa.login IN
      (
        'dresuncuche',
        'staphagtiostooz',
        'sleekefclanfeu',
        'inanexdean',
        'tiborguitwor',
        'vijnofebdo',
        'dingvalrudep',
        'necnawhire',
        'katy5452',
        'choisosale',
        'destnoumerly',
        'tansurpsendcam',
        'heartclevnica',
        'mercarogar',
        'disfdeberpo',
        'kamicfeici',
        'glutaqdymil',
        'terticora',
        'zithrinidi',
        'gausithirshu',
        'mantnacina',
        'battryvorma',
        'fighheacentri',
        'brousoutmino',
        'swabmicquosu',
        'encatitu',
        'imphocogre',
        'ivenganes',
        'ceiceerekul',
        'highcommoucha',
        'inhenfeverb',
        'lapneucewo',
        'worvolksanca',
        'lecnaribna',
        'geocintingprin',
        'belihinknusc',
        'practerrero',
        'olanomvi',
        'boabekimoun',
        'ibatelzu1984'
      )
      AND tag_id = (SELECT id
                    FROM tag
                    WHERE code = 'RELIABLE')
  );

-- выбрать тег БЛОК и записи акков с заданными логинами
SELECT
  (
    SELECT code
    FROM tag
    WHERE code = 'BLOCK'),
  a.*
FROM
  account a
WHERE
  a.login IN
  (
    'dresuncuche',
    'staphagtiostooz',
    'sleekefclanfeu',
    'inanexdean',
    'tiborguitwor',
    'vijnofebdo',
    'dingvalrudep',
    'necnawhire',
    'katy5452',
    'choisosale',
    'destnoumerly',
    'tansurpsendcam',
    'heartclevnica',
    'mercarogar',
    'disfdeberpo',
    'kamicfeici',
    'glutaqdymil',
    'terticora',
    'zithrinidi',
    'gausithirshu',
    'mantnacina',
    'battryvorma',
    'fighheacentri',
    'brousoutmino',
    'swabmicquosu',
    'encatitu',
    'imphocogre',
    'ivenganes',
    'ceiceerekul',
    'highcommoucha',
    'inhenfeverb',
    'lapneucewo',
    'worvolksanca',
    'lecnaribna',
    'geocintingprin',
    'belihinknusc',
    'practerrero',
    'olanomvi',
    'boabekimoun',
    'ibatelzu1984'
  );

SELECT
  (
    SELECT id
    FROM tag
    WHERE code = 'BLOCK') AS tag_id,
  a.id                    AS a_id
FROM
  account a
WHERE
  a.login IN
  (
    'dresuncuche',
    'staphagtiostooz',
    'sleekefclanfeu',
    'inanexdean',
    'tiborguitwor',
    'vijnofebdo',
    'dingvalrudep',
    'necnawhire',
    'katy5452',
    'choisosale',
    'destnoumerly',
    'tansurpsendcam',
    'heartclevnica',
    'mercarogar',
    'disfdeberpo',
    'kamicfeici',
    'glutaqdymil',
    'terticora',
    'zithrinidi',
    'gausithirshu',
    'mantnacina',
    'battryvorma',
    'fighheacentri',
    'brousoutmino',
    'swabmicquosu',
    'encatitu',
    'imphocogre',
    'ivenganes',
    'ceiceerekul',
    'highcommoucha',
    'inhenfeverb',
    'lapneucewo',
    'worvolksanca',
    'lecnaribna',
    'geocintingprin',
    'belihinknusc',
    'practerrero',
    'olanomvi',
    'boabekimoun',
    'ibatelzu1984'
  );

INSERT INTO tag_account
(tag_id, account_id)
  SELECT
    (
      SELECT id
      FROM tag
      WHERE code = 'BLOCK') AS tag_id,
    a.id                    AS a_id
  FROM
    account a
  WHERE
    a.login IN
    (
      'dresuncuche',
      'staphagtiostooz',
      'sleekefclanfeu',
      'inanexdean',
      'tiborguitwor',
      'vijnofebdo',
      'dingvalrudep',
      'necnawhire',
      'katy5452',
      'choisosale',
      'destnoumerly',
      'tansurpsendcam',
      'heartclevnica',
      'mercarogar',
      'disfdeberpo',
      'kamicfeici',
      'glutaqdymil',
      'terticora',
      'zithrinidi',
      'gausithirshu',
      'mantnacina',
      'battryvorma',
      'fighheacentri',
      'brousoutmino',
      'swabmicquosu',
      'encatitu',
      'imphocogre',
      'ivenganes',
      'ceiceerekul',
      'highcommoucha',
      'inhenfeverb',
      'lapneucewo',
      'worvolksanca',
      'lecnaribna',
      'geocintingprin',
      'belihinknusc',
      'practerrero',
      'olanomvi',
      'boabekimoun',
      'ibatelzu1984'
    );

SELECT to_char(row_number()
               OVER (), '999999') ||
       ' http://'
       || -- proxy_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ':'
       || -- proxy_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || '@'
       || -- proxy_address
       (SELECT s.code
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- mail_login
       (SELECT sa.login
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'MAIL_RU')
       || ' '
       || -- mail_password
       (SELECT sa.password
        FROM person ps
          JOIN person_account psa ON ps.id = psa.person_id
          JOIN account sa ON psa.account_id = sa.id
          JOIN service s ON sa.service_id = s.id
        WHERE ps.id = p.id AND s.code = 'MAIL_RU')
       || ' '
       || -- account tag
       (
         SELECT t.code
         FROM person ps
           JOIN person_account psa ON ps.id = psa.person_id
           JOIN account sa ON psa.account_id = sa.id
           JOIN tag_account tsa
             ON sa.id = tsa.account_id
           JOIN tag t
             ON tsa.tag_id = t.id
         WHERE ps.id = p.id AND t.code = 'BLOCK'
       )
  AS person_account
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN tag_account tsa
          ON sa.id = tsa.account_id
        JOIN tag t
          ON tsa.tag_id = t.id
      WHERE
        ps.id = p.id
        AND t.code = 'BLOCK'
        AND tsa.insert_date > to_timestamp('21 Jun 2017', 'DD Mon YYYY')
  )
ORDER BY p.id;

SELECT *
FROM person ps
  JOIN person_account psa ON ps.id = psa.person_id
  JOIN account sa ON psa.account_id = sa.id
  JOIN tag_account tsa
    ON sa.id = tsa.account_id
  JOIN tag t
    ON tsa.tag_id = t.id
WHERE
  t.code = 'BLOCK'
  AND tsa.insert_date < to_timestamp('21 Jun 2017', 'DD Mon YYYY');

DELETE FROM tag_account
WHERE tag_id = (SELECT id
                FROM tag
                WHERE code = 'PROMO_ME');

-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'kazuc'
  )
  AND a.service_id = (
    SELECT id
    FROM service
    WHERE code = 'PIKABU_RU'
  );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'kazuc'
      )
      AND a.service_id = (
        SELECT id
        FROM service
        WHERE code = 'PIKABU_RU'
      )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'kazuc'
      )
      AND a.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
  );

-- удалить аккаунты
DELETE FROM account
WHERE login IN
      (
        'kazuc'
      )
      AND service_id = (
  SELECT id
  FROM service
  WHERE code = 'PIKABU_RU');

-- выбрать Пикабушников без тегов ( то есть это и не Существенные и не Блокированные )
SELECT *
FROM
  account a
  LEFT JOIN tag_account ta
    ON a.id = ta.account_id
WHERE
  ta.id IS NULL
  AND a.service_id =
      (
        SELECT id
        FROM service
        WHERE code = 'PIKABU_RU'
      )
  AND a.is_hidden = FALSE;

-- выбрать аккаунты с заданным паролем
SELECT *
FROM
  account a
WHERE a.password = '92PfGt7HpNQrAokMdeMd';

INSERT INTO person (code) VALUES ('vertex4te');

INSERT INTO person_account (person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'vertex4te'),
    (SELECT id
     FROM account
     WHERE login = 'vertex4' AND service_id = (SELECT id
                                               FROM service
                                               WHERE code = 'PIKABU_RU'));

INSERT INTO person_account (person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'vertex4te'),
    (SELECT id
     FROM account
     WHERE login = 'vertex4@inbox.ru' AND service_id = (SELECT id
                                                        FROM service
                                                        WHERE code = 'MAIL_RU'));

SELECT *
FROM service
WHERE code = '146.185.249.106:24531';

SELECT *
FROM account
WHERE service_id = (SELECT id
                    FROM service
                    WHERE code = '146.185.249.106:24531');

INSERT INTO person_account (person_id, account_id)
  SELECT
    (SELECT id
     FROM person
     WHERE code = 'vertex4te'),
    (SELECT id
     FROM account
     WHERE service_id = (SELECT id
                         FROM service
                         WHERE code = '146.185.249.106:24531'));

-- выбрать тег 'RELIABLE' и заданные аккаунты
SELECT
  (SELECT code
   FROM tag
   WHERE code = 'RELIABLE'),
  a.login
FROM
  account a
WHERE
  a.login IN
  (
    'vertex4'
  );

-- выбрать айди для тега 'RELIABLE' и айди для заданных аккаунтов
SELECT
  (SELECT id
   FROM tag
   WHERE code = 'RELIABLE'),
  a.id
FROM
  account a
WHERE
  a.login IN
  (
    'vertex4'
  );

-- проставить тег 'RELIABLE' для заданных аккаунтов
INSERT INTO tag_account (tag_id, account_id)
  SELECT
    (SELECT id
     FROM tag
     WHERE code = 'RELIABLE'),
    a.id
  FROM
    account a
  WHERE
    a.login IN
    (
      'vertex4'
    );

-- выбрать Пикабушников без тегов ( то есть это и не Существенные и не Блокированные )
SELECT *
FROM
  account a
  LEFT JOIN tag_account ta
    ON a.id = ta.account_id
WHERE
  ta.id IS NULL
  AND a.service_id =
      (
        SELECT id
        FROM service
        WHERE code = 'PIKABU_RU'
      )
  AND a.is_hidden = FALSE;

SELECT
  to_char(row_number()
          OVER (), '999999') ||
  ' http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- account tag
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  )
    AS person_account,
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  ) AS account_tag
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
ORDER BY account_tag DESC ;