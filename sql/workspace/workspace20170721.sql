SELECT
  *
FROM
  account a
JOIN tag_account ta
  on a.id = ta.account_id
RIGHT JOIN tag t
    on ta.tag_id = t.id
where a.id = 525 or ta.account_id IS NULL
;

SELECT
  *
FROM
  tag_account ta
RIGHT JOIN tag t
  on t.id = ta.tag_id
where ta.account_id = 525 or ta.account_id IS NULL
;

SELECT
  *
FROM
  tag t
LEFT JOIN tag_account ta
  on t.id = ta.tag_id
RIGHT JOIN account a
    on ta.account_id = a.id
where a.id = 525 or a.id IS NULL
;

SELECT
  *
FROM
  tag t
WHERE
  NOT EXISTS(SELECT NULL FROM tag_account ta WHERE ta.tag_id = t.id AND ta.account_id = 525)
;
