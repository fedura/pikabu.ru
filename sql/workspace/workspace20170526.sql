SELECT *
FROM
  service_account sa
WHERE
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.id = sa.id
  );

-- unused proxy
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id
WHERE
  t.code = 'PROXY'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

-- unused @mail.ru
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
WHERE
  s.code = 'MAIL_RU'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

-- unused pikabu.ru
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
WHERE
  s.code = 'PIKABU_RU'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );


DELETE FROM service_account
WHERE
  id IN
  (
    SELECT sa.id
    FROM
      service_account sa
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND
      NOT Exists
      (
          SELECT NULL
          FROM
            person_service_account psa
          WHERE
            psa.service_account_id = sa.id
      )
  );

SELECT *
FROM
  person
WHERE
  code IN
  (
    'kohgutep',
    'katy5452',
    'kiriknik',
    'kivido',
    'kapamel1k',
    'kirl0g'
  );

SELECT *
FROM
  person p
  JOIN person_service_account psa
    ON p.id = psa.person_id
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  LEFT JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  LEFT JOIN tag ta
    ON tsa.tag_id = ta.id
  JOIN service s
    ON sa.service_id = s.id
WHERE
  p.code IN
  (
    'kohgutep',
    'katy5452',
    'kiriknik',
    'kivido',
    'kapamel1k',
    'kirl0g'
  );

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT (SELECT sa.login
        FROM service_account sa
          JOIN tag_service_account tsa
            ON sa.id = tsa.service_account_id
          JOIN tag t
            ON tsa.tag_id = t.id
        WHERE sa.service_id = (SELECT id
                               FROM service
                               WHERE code = 'PIKABU_RU')
              AND t.code = 'RELIABLE'
              AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_service_account psae
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE psae.person_id =
                  (
                    SELECT psas.person_id
                    FROM person_service_account psas
                    WHERE psas.service_account_id = sa.id
                  )
                  AND te.code = 'PROXY'

        )
        OFFSET r.i
        LIMIT 1) AS account_login
FROM
  r
WHERE
  (SELECT sa.login
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) IS NOT NULL;

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (SELECT sa.login
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) AS account_login,
  (SELECT s.code
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy,
  (SELECT sa.login
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy_login
FROM
  r
WHERE
  (SELECT sa.login
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) IS NOT NULL;

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (SELECT (SELECT code
           FROM person
           WHERE id = psa.person_id)
   FROM
     service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
     JOIN person_service_account psa
       ON sa.id = psa.service_account_id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) AS account_login,
  (SELECT s.code
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy,
  (SELECT sa.login
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy_login
FROM
  r
WHERE
  (SELECT sa.id
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) IS NOT NULL;

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (SELECT psa.person_id
   FROM
     service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
     JOIN person_service_account psa
       ON sa.id = psa.service_account_id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) AS person_id,
  (SELECT sa.id
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy_account
FROM
  r
WHERE
  (SELECT sa.id
   FROM service_account sa
     JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id
     JOIN tag t
       ON tsa.tag_id = t.id
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND t.code = 'RELIABLE'
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) IS NOT NULL;

INSERT INTO person_service_account (person_id, service_account_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (SELECT psa.person_id
     FROM
       service_account sa
       JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id
       JOIN tag t
         ON tsa.tag_id = t.id
       JOIN person_service_account psa
         ON sa.id = psa.service_account_id
     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND t.code = 'RELIABLE'
           AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psae
           JOIN service_account sae
             ON psae.service_account_id = sae.id
           JOIN service se
             ON sae.service_id = se.id
           JOIN tag_service tse
             ON se.id = tse.service_id
           JOIN tag te
             ON tse.tag_id = te.id
         WHERE psae.person_id =
               (
                 SELECT psas.person_id
                 FROM person_service_account psas
                 WHERE psas.service_account_id = sa.id
               )
               AND te.code = 'PROXY'

     )
     OFFSET r.i
     LIMIT 1) AS person_id,
    (SELECT sa.id
     FROM
       service_account sa
       JOIN service s
         ON sa.service_id = s.id
       JOIN tag_service ts
         ON s.id = ts.service_id
       JOIN tag t
         ON ts.tag_id = t.id
     WHERE
       t.code = 'PROXY'
       AND NOT EXISTS
       (
           SELECT NULL
           FROM
             person_service_account psa
           WHERE
             psa.service_account_id = sa.id
       )
     OFFSET r.i
     LIMIT 1) AS proxy_account
  FROM
    r
  WHERE
    (SELECT sa.id
     FROM service_account sa
       JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id
       JOIN tag t
         ON tsa.tag_id = t.id
     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND t.code = 'RELIABLE'
           AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psae
           JOIN service_account sae
             ON psae.service_account_id = sae.id
           JOIN service se
             ON sae.service_id = se.id
           JOIN tag_service tse
             ON se.id = tse.service_id
           JOIN tag te
             ON tse.tag_id = te.id
         WHERE psae.person_id =
               (
                 SELECT psas.person_id
                 FROM person_service_account psas
                 WHERE psas.service_account_id = sa.id
               )
               AND te.code = 'PROXY'

     )
     OFFSET r.i
     LIMIT 1) IS NOT NULL;

SELECT count(*)
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'kamelius98',
    'killerhead',
    'kemkir',
    'klaudalert',
    'kilovad',
    'khazibula',
    'kiwi7295',
    'klyuevand',
    'kail68',
    'kalyany4',
    'katonov',
    'kirill014',
    'relyntursvac',
    'distlalvalldis',
    'etaronla',
    'khosmairolsau',
    'abgeexvilect',
    'maldaticdie',
    'greasnonvihe',
    'quibertesin',
    'quanthemptentgher',
    'matolltreatfunc',
    'cacesneugres'
  );

SELECT *
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
WHERE
  sa.login IN
  (
    'kamelius98',
    'killerhead',
    'kemkir',
    'klaudalert',
    'kilovad',
    'khazibula',
    'kiwi7295',
    'klyuevand',
    'kail68',
    'kalyany4',
    'katonov',
    'kirill014',
    'relyntursvac',
    'distlalvalldis',
    'etaronla',
    'khosmairolsau',
    'abgeexvilect',
    'maldaticdie',
    'greasnonvihe',
    'quibertesin',
    'quanthemptentgher',
    'matolltreatfunc',
    'cacesneugres'
  );

DELETE FROM person_service_account
WHERE
  service_account_id IN
  (
    SELECT id
    FROM
      service_account sa
    WHERE
      sa.login IN
      (
        'kamelius98',
        'killerhead',
        'kemkir',
        'klaudalert',
        'kilovad',
        'khazibula',
        'kiwi7295',
        'klyuevand',
        'kail68',
        'kalyany4',
        'katonov',
        'kirill014',
        'relyntursvac',
        'distlalvalldis',
        'etaronla',
        'khosmairolsau',
        'abgeexvilect',
        'maldaticdie',
        'greasnonvihe',
        'quibertesin',
        'quanthemptentgher',
        'matolltreatfunc',
        'cacesneugres'
      )
  );

DELETE FROM service_account
WHERE login IN (
  'kamelius98',
  'killerhead',
  'kemkir',
  'klaudalert',
  'kilovad',
  'khazibula',
  'kiwi7295',
  'klyuevand',
  'kail68',
  'kalyany4',
  'katonov',
  'kirill014',
  'relyntursvac',
  'distlalvalldis',
  'etaronla',
  'khosmairolsau',
  'abgeexvilect',
  'maldaticdie',
  'greasnonvihe',
  'quibertesin',
  'quanthemptentgher',
  'matolltreatfunc',
  'cacesneugres'
);

SELECT *
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  JOIN service s
    ON sa.service_id = s.id
WHERE
  NOT exists
  (
      SELECT NULL
      FROM
        person_service_account psae
        JOIN service_account sae
          ON psae.service_account_id = sae.id
        JOIN service se
          ON sae.service_id = se.id
      WHERE
        psae.person_id = psa.person_id
        AND se.code = 'PIKABU_RU'
  );

DELETE FROM person_service_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_service_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_service_account psae
                JOIN service_account sae
                  ON psae.service_account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

SELECT *
FROM
  person p
  LEFT JOIN person_service_account psa
    ON p.id = psa.person_id
WHERE
  psa.id IS NULL;

DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_service_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

SELECT count(*)
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'vasilek88',
    'britvas',
    'kartcent',
    'katiish',
    'kamazzz21',
    'kipriano',
    'kap0ne',
    'kimenhack',
    'kblc9l',
    'killer9171',
    'kat3959',
    'kit9433',
    'kl9n',
    'kaka4a',
    'kbozdik',
    'killoch',
    'kirklim',
    'grinnestcusob'
  );

SELECT *
FROM tag t
WHERE
  t.code = 'BLOCK';

SELECT
  sa.login,
  (SELECT t.code
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'vasilek88',
    'britvas',
    'kartcent',
    'katiish',
    'kamazzz21',
    'kipriano',
    'kap0ne',
    'kimenhack',
    'kblc9l',
    'killer9171',
    'kat3959',
    'kit9433',
    'kl9n',
    'kaka4a',
    'kbozdik',
    'killoch',
    'kirklim',
    'grinnestcusob'
  );

SELECT
  sa.id,
  (SELECT t.id
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'vasilek88',
    'britvas',
    'kartcent',
    'katiish',
    'kamazzz21',
    'kipriano',
    'kap0ne',
    'kimenhack',
    'kblc9l',
    'killer9171',
    'kat3959',
    'kit9433',
    'kl9n',
    'kaka4a',
    'kbozdik',
    'killoch',
    'kirklim',
    'grinnestcusob'
  );

INSERT INTO tag_service_account (service_account_id, tag_id)
  SELECT
    sa.id,
    (SELECT t.id
     FROM tag t
     WHERE
       t.code = 'BLOCK')
  FROM
    service_account sa
  WHERE
    sa.login IN
    (
      'vasilek88',
      'britvas',
      'kartcent',
      'katiish',
      'kamazzz21',
      'kipriano',
      'kap0ne',
      'kimenhack',
      'kblc9l',
      'killer9171',
      'kat3959',
      'kit9433',
      'kl9n',
      'kaka4a',
      'kbozdik',
      'killoch',
      'kirklim',
      'grinnestcusob'
    );

-- account without tag
SELECT count(*)
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
  LEFT JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
WHERE
  s.code = 'PIKABU_RU'
  AND tsa.id IS NULL;

SELECT count(*)
FROM service_account sa
  JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  JOIN tag t
    ON tsa.tag_id = t.id
WHERE sa.service_id = (SELECT id
                       FROM service
                       WHERE code = 'PIKABU_RU')
      AND t.code IN ('RELIABLE', 'BLOCK');

SELECT count(*)
FROM service_account sa
  JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  JOIN tag t
    ON tsa.tag_id = t.id
WHERE sa.service_id = (SELECT id
                       FROM service
                       WHERE code = 'PIKABU_RU')
      AND t.code IN ('RELIABLE');

SELECT count(*)
FROM service_account sa
  JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  JOIN tag t
    ON tsa.tag_id = t.id
WHERE sa.service_id = (SELECT id
                       FROM service
                       WHERE code = 'PIKABU_RU')
      AND t.code IN ('BLOCK');

-- persons have no proxy
WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (SELECT psa.person_id
   FROM
     service_account sa
     JOIN person_service_account psa
       ON sa.id = psa.service_account_id
     LEFT JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id

   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND tsa.id IS NULL
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) AS person_id,
  (SELECT sa.id
   FROM
     service_account sa
     JOIN service s
       ON sa.service_id = s.id
     JOIN tag_service ts
       ON s.id = ts.service_id
     JOIN tag t
       ON ts.tag_id = t.id
   WHERE
     t.code = 'PROXY'
     AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psa
         WHERE
           psa.service_account_id = sa.id
     )
   OFFSET r.i
   LIMIT 1) AS proxy_account
FROM
  r
WHERE
  (SELECT psa.person_id
   FROM
     service_account sa
     JOIN person_service_account psa
       ON sa.id = psa.service_account_id
     LEFT JOIN tag_service_account tsa
       ON sa.id = tsa.service_account_id

   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
         AND tsa.id IS NULL
         AND NOT EXISTS
   (
       SELECT NULL
       FROM
         person_service_account psae
         JOIN service_account sae
           ON psae.service_account_id = sae.id
         JOIN service se
           ON sae.service_id = se.id
         JOIN tag_service tse
           ON se.id = tse.service_id
         JOIN tag te
           ON tse.tag_id = te.id
       WHERE psae.person_id =
             (
               SELECT psas.person_id
               FROM person_service_account psas
               WHERE psas.service_account_id = sa.id
             )
             AND te.code = 'PROXY'

   )
   OFFSET r.i
   LIMIT 1) IS NOT NULL;

INSERT INTO person_service_account (person_id, service_account_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (SELECT psa.person_id
     FROM
       service_account sa
       JOIN person_service_account psa
         ON sa.id = psa.service_account_id
       LEFT JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id

     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND tsa.id IS NULL
           AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psae
           JOIN service_account sae
             ON psae.service_account_id = sae.id
           JOIN service se
             ON sae.service_id = se.id
           JOIN tag_service tse
             ON se.id = tse.service_id
           JOIN tag te
             ON tse.tag_id = te.id
         WHERE psae.person_id =
               (
                 SELECT psas.person_id
                 FROM person_service_account psas
                 WHERE psas.service_account_id = sa.id
               )
               AND te.code = 'PROXY'

     )
     OFFSET r.i
     LIMIT 1) AS person_id,
    (SELECT sa.id
     FROM
       service_account sa
       JOIN service s
         ON sa.service_id = s.id
       JOIN tag_service ts
         ON s.id = ts.service_id
       JOIN tag t
         ON ts.tag_id = t.id
     WHERE
       t.code = 'PROXY'
       AND NOT EXISTS
       (
           SELECT NULL
           FROM
             person_service_account psa
           WHERE
             psa.service_account_id = sa.id
       )
     OFFSET r.i
     LIMIT 1) AS proxy_account
  FROM
    r
  WHERE
    (SELECT psa.person_id
     FROM
       service_account sa
       JOIN person_service_account psa
         ON sa.id = psa.service_account_id
       LEFT JOIN tag_service_account tsa
         ON sa.id = tsa.service_account_id

     WHERE sa.service_id = (SELECT id
                            FROM service
                            WHERE code = 'PIKABU_RU')
           AND tsa.id IS NULL
           AND NOT EXISTS
     (
         SELECT NULL
         FROM
           person_service_account psae
           JOIN service_account sae
             ON psae.service_account_id = sae.id
           JOIN service se
             ON sae.service_id = se.id
           JOIN tag_service tse
             ON se.id = tse.service_id
           JOIN tag te
             ON tse.tag_id = te.id
         WHERE psae.person_id =
               (
                 SELECT psas.person_id
                 FROM person_service_account psas
                 WHERE psas.service_account_id = sa.id
               )
               AND te.code = 'PROXY'

     )
     OFFSET r.i
     LIMIT 1) IS NOT NULL;

SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')     AS proxy,

  -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU') AS account_login,
  -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU') AS account_password,
  -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_login,
  -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        LEFT JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU' AND tsa.id IS NULL
  )
ORDER BY p.id;

SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        LEFT JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU' AND tsa.id IS NULL
  )
ORDER BY p.id;

-- RELIABLE
SELECT
  sa.login,
  (SELECT t.code
   FROM tag t
   WHERE
     t.code = 'RELIABLE')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'necnawhire',
    'destnoumerly',
    'practerrero',
    'olanomvi',
    'disfdeberpo',
    'tansurpsendcam'
  );

INSERT INTO tag_service_account (service_account_id, tag_id)
    SELECT
  sa.id,
  (SELECT t.id
   FROM tag t
   WHERE
     t.code = 'RELIABLE')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'necnawhire',
    'destnoumerly',
    'practerrero',
    'olanomvi',
    'disfdeberpo',
    'tansurpsendcam'
  );

-- BLOCK
SELECT
  sa.login,
  (SELECT t.code
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'kama0590',
    'kargintima',
    'kaisserhaff',
    'kiv89',
    'kenenbek',
    'koly4ka001',
    'karasov',
    'kaban7610',
    'kartez2000',
    'kid007',
    'katya12304',
    'karkyshka',
    'kennympei',
    'kazuc',
    'kisik3',
    'kempriol',
    'etepfici'
  );

INSERT INTO tag_service_account (service_account_id, tag_id)
    SELECT
  sa.id,
  (SELECT t.id
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'kama0590',
    'kargintima',
    'kaisserhaff',
    'kiv89',
    'kenenbek',
    'koly4ka001',
    'karasov',
    'kaban7610',
    'kartez2000',
    'kid007',
    'katya12304',
    'karkyshka',
    'kennympei',
    'kazuc',
    'kisik3',
    'kempriol',
    'etepfici'
  );

-- FAIL
SELECT count(*)
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'killbis',
    'vilrebolo'
  );

SELECT *
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
WHERE
  sa.login IN
  (
    'killbis',
    'vilrebolo'
  );

DELETE FROM person_service_account
WHERE
  service_account_id IN
  (
    SELECT id
    FROM
      service_account sa
    WHERE
      sa.login IN
      (
    'killbis',
    'vilrebolo'
      )
  );

DELETE FROM service_account
WHERE login IN (
    'killbis',
    'vilrebolo'
);

SELECT *
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  JOIN service s
    ON sa.service_id = s.id
WHERE
  NOT exists
  (
      SELECT NULL
      FROM
        person_service_account psae
        JOIN service_account sae
          ON psae.service_account_id = sae.id
        JOIN service se
          ON sae.service_id = se.id
      WHERE
        psae.person_id = psa.person_id
        AND se.code = 'PIKABU_RU'
  );

DELETE FROM person_service_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_service_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_service_account psae
                JOIN service_account sae
                  ON psae.service_account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

SELECT *
FROM
  person p
  LEFT JOIN person_service_account psa
    ON p.id = psa.person_id
WHERE
  psa.id IS NULL;

DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_service_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

-- BLOCK
SELECT
  sa.login,
  (SELECT t.code
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'katterham',
    'kenny106'
  );

INSERT INTO tag_service_account (service_account_id, tag_id)
    SELECT
  sa.id,
  (SELECT t.id
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'katterham',
    'kenny106'
  );

SELECT
  sa.id,
  (SELECT t.id
   FROM tag t
   WHERE
     t.code = 'BLOCK')
FROM
  service_account sa
WHERE
  sa.login IN
  (
    'borisserb',
    'viktorvor',
    'aleksandrkurn',
    'katashe131',
    'petras',
    'leonid5',
    'mikhaildb',
    'igor9mm'
  );

INSERT INTO tag_service_account (service_account_id, tag_id)
  SELECT
    sa.id,
    (SELECT t.id
     FROM tag t
     WHERE
       t.code = 'BLOCK')
  FROM
    service_account sa
  WHERE
    sa.login IN
    (
    'borisserb',
    'viktorvor',
    'aleksandrkurn',
    'katashe131',
    'petras',
    'leonid5',
    'mikhaildb',
    'igor9mm'
    );

SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
        join tag t
        on tsa.tag_id = t.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU' AND t.code ='RELIABLE'
  )
ORDER BY p.id;

SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY') as proxy,
  -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU'),
  -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU'),
  -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU'),
  -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password,
  -- account_status
  (SELECT t.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     left join tag_service_account tsa on sa.id = tsa.service_account_id
     left join tag t on tsa.tag_id = t.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
        join tag t
        on tsa.tag_id = t.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU' AND t.code ='RELIABLE'
  )
ORDER BY p.id;