DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ( 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'kirl0g'
          )
      );

INSERT INTO
  tag_account
  (tag_id, account_id)
    SELECT
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('BLOCK')
      ),
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'kirl0g'
          )
      )
;


SELECT *
FROM
  account a
WHERE
  a.login ILIKE '%Gulftown%';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'Gulftown',
    'gulftown@bk.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '146.185.249.123:24531%';

INSERT INTO person
(code) VALUES ('Gulftown');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'Gulftown'),
   (SELECT id
    FROM account
    WHERE login = 'Gulftown')),
  ((SELECT id
    FROM person
    WHERE code = 'Gulftown'),
   (SELECT id
    FROM account
    WHERE login = 'gulftown@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Gulftown'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '146.185.249.123:24531%'));

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'Gulftown'
      )
)
SELECT
  acc.account_id,
  (SELECT login
   FROM account
   WHERE id = acc.account_id),
  t.id,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'Gulftown'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );