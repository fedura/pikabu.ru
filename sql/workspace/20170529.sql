-- сформировать строку для использования аккаунта в ЗеноПостере
SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY') as proxy,
  -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU'),
  -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU'),
  -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU'),
  -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password,
  -- account_status
  (SELECT t.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     left join tag_service_account tsa on sa.id = tsa.service_account_id
     left join tag t on tsa.tag_id = t.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU') as account_status
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
        join tag t
        on tsa.tag_id = t.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
ORDER BY account_status;

SELECT
  'http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_service_account psa ON ps.id = psa.person_id
     JOIN service_account sa ON psa.service_account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')   AS mail_password
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_service_account psa ON ps.id = psa.person_id
        JOIN service_account sa ON psa.service_account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
        join tag t
        on tsa.tag_id = t.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
ORDER BY p.id;