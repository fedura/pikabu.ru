SELECT
  a.login
FROM
 tag t
JOIN tag_account ta
  ON t.id = ta.tag_id
JOIN account a
   ON ta.account_id = a.id
WHERE
  t.code = 'RELIABLE'
;

SELECT
  *
FROM
  account a
  JOIN tag_account ta
  ON a.id = ta.account_id
  JOIN tag t
    ON ta.tag_id = t.id
WHERE
  login = 'Arabat';

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'Arabat'
          )
      )
RETURNING id;

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'Arabat'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'BLOCK'
    )
RETURNING account_id, tag_id;

DELETE FROM
  tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code IN ('PROMO_ME', 'RELIABLE')
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN
          (
            'e.Jonny'
          )
      )
RETURNING id;

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'e.Jonny'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'BLOCK'
    )
RETURNING account_id, tag_id;
