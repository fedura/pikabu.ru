SELECT
  (SELECT login
   FROM account
   WHERE id = ta.account_id),
  (SELECT code
   FROM tag
   WHERE id = ta.tag_id)
FROM
  tag_account ta
WHERE
  ta.tag_id =
  (
    SELECT id
    FROM
      tag
    WHERE
      code = 'PROMO_ME'
  )
  AND
  ta.account_id IN
  (
    SELECT id
    FROM
      account
    WHERE
      login IN ('kiriknik', 'ocelabex')
  );

DELETE FROM tag_account
WHERE id IN (SELECT id
             FROM
               tag_account ta
             WHERE
               ta.tag_id =
               (
                 SELECT id
                 FROM
                   tag
                 WHERE
                   code = 'PROMO_ME'
               )
               AND
               ta.account_id IN
               (
                 SELECT id
                 FROM
                   account
                 WHERE
                   login IN ('kiriknik', 'ocelabex')
               )
);

SELECT *
FROM account_post
WHERE insert_date > '2017-06-29';

UPDATE post
SET is_hidden = TRUE
WHERE id IN (SELECT post_id
             FROM account_post
             WHERE insert_date > '2017-06-29');

DELETE FROM account_post
WHERE insert_date > '2017-06-29';

DELETE FROM tag_account
WHERE tag_id IN
      (
        SELECT id
        FROM
          tag
        WHERE
          code = 'PROMO_ME'
      )
      AND
      account_id IN
      (
        SELECT id
        FROM
          account
        WHERE
          login IN ('abipsdatun', 'rarecrepa', 'kivido', 'vertex4te')
      );

SELECT *
FROM
  account
WHERE
  login IN ('abipsdatun', 'rarecrepa', 'kivido', 'vertex4te');

SELECT
  a.login AS login,
  a.id    AS id
FROM
  account a
WHERE
  EXISTS(
      SELECT NULL
      FROM tag t
        JOIN tag_account ta ON t.id = ta.tag_id
      WHERE ta.account_id = a.id AND t.code = 'RELIABLE'
  )
  AND
  EXISTS(
      SELECT NULL
      FROM tag t
        JOIN tag_account ta ON t.id = ta.tag_id
      WHERE ta.account_id = a.id AND t.code = 'PROMO_ME'
  );

SELECT
  p.id        AS post_id,
  p.title     AS post_title,
  p.body      AS post_body,
  p.bulk_tags AS post_tags
FROM
  post p
  LEFT JOIN account_post ap
    ON p.id = ap.post_id
WHERE
  p.is_hidden IS TRUE
--AND ap.id IS NULL
--LIMIT 5
;

SELECT *
FROM account_post ap
  JOIN post p ON ap.post_id = p.id
WHERE p.is_hidden = TRUE;

DELETE FROM account_post
WHERE id IN (SELECT ap.id
             FROM account_post ap
               JOIN post p ON ap.post_id = p.id
             WHERE p.is_hidden = TRUE);

SELECT
  p.id        AS post_id,
  p.title     AS post_title,
  p.body      AS post_body,
  p.bulk_tags AS post_tags
FROM
  post p
  LEFT JOIN account_post ap
    ON p.id = ap.post_id
WHERE
  p.is_hidden IS TRUE
  AND ap.id IS NULL
LIMIT 5;


SELECT *
FROM
  account a
WHERE
  a.login IN (
    'dimitarserg',
    'dimitarserg@bk.ru'
  );

SELECT *
FROM
  account a
WHERE
  a.login ILIKE 'dimitarserg';
-- DimitarSerg

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'DimitarSerg',
    'dimitarserg@bk.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
WHERE
  s.code LIKE '185.139.215.15:24531%';

SELECT *
FROM person
WHERE code ILIKE 'DimitarSerg';

INSERT INTO person
(code) VALUES ('DimitarSerg');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'DimitarSerg'),
   (SELECT id
    FROM account
    WHERE login = 'DimitarSerg')),
  ((SELECT id
    FROM person
    WHERE code = 'DimitarSerg'),
   (SELECT id
    FROM account
    WHERE login = 'dimitarserg@bk.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'DimitarSerg'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
    WHERE
      s.code LIKE '185.139.215.15:24531%'));

SELECT *
FROM
  account a
WHERE
  a.login ILIKE 'feuerloescher';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'feuerloescher',
    'feuerloescher@list.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '94.26.195.88:24531%';

INSERT INTO person
(code) VALUES ('feuerloescher');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'feuerloescher'),
   (SELECT id
    FROM account
    WHERE login = 'feuerloescher')),
  ((SELECT id
    FROM person
    WHERE code = 'feuerloescher'),
   (SELECT id
    FROM account
    WHERE login = 'feuerloescher@list.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'feuerloescher'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '94.26.195.88:24531%'));

SELECT *
FROM
  account a
WHERE
  a.login ILIKE 'watsonrus';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'WatsonRus',
    'watsonrus@list.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '94.26.195.136:24531%';

INSERT INTO person
(code) VALUES ('WatsonRus');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'WatsonRus'),
   (SELECT id
    FROM account
    WHERE login = 'WatsonRus')),
  ((SELECT id
    FROM person
    WHERE code = 'WatsonRus'),
   (SELECT id
    FROM account
    WHERE login = 'watsonrus@list.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'WatsonRus'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '94.26.195.136:24531%'));

-- nickontoluca@mail.ru

SELECT *
FROM
  account a
WHERE
  a.login ILIKE 'nickontoluca';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'NickOnToluca',
    'nickontoluca@mail.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '94.26.192.155:24531%';

INSERT INTO person
(code) VALUES ('NickOnToluca');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'NickOnToluca'),
   (SELECT id
    FROM account
    WHERE login = 'NickOnToluca')),
  ((SELECT id
    FROM person
    WHERE code = 'NickOnToluca'),
   (SELECT id
    FROM account
    WHERE login = 'nickontoluca@mail.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'NickOnToluca'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '94.26.192.155:24531%'));

SELECT *
FROM
  account a
WHERE
  a.login ILIKE 'denhead';

SELECT *
FROM
  account a
WHERE
  a.login IN (
    'Denhead',
    'denhead@list.ru'
  );

SELECT *
FROM
  account a
  JOIN service s
    ON a.service_id = s.id
  LEFT JOIN person_account pa ON
                                a.id = pa.account_id
WHERE
  pa.id IS NULL
  AND s.code LIKE '94.26.192.205:24531%';

INSERT INTO person
(code) VALUES ('Denhead');

INSERT INTO person_account (person_id, account_id)
VALUES
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT id
    FROM account
    WHERE login = 'Denhead')),
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT id
    FROM account
    WHERE login = 'denhead@list.ru')),
  ((SELECT id
    FROM person
    WHERE code = 'Denhead'),
   (SELECT a.id
    FROM
      account a
      JOIN service s
        ON a.service_id = s.id
      LEFT JOIN person_account pa ON
                                    a.id = pa.account_id
    WHERE
      pa.id IS NULL
      AND s.code LIKE '94.26.192.205:24531%'));

SELECT *
FROM
  account a
  JOIN tag_account ta
    ON a.id = ta.account_id
  JOIN tag t
    ON ta.tag_id = t.id
WHERE
  t.code = 'BLOCK';

SELECT id
FROM
  account a
WHERE
  a.login IN
  (
    'gausithirshu',
    'ibatelzu1984',
    'belihinknusc',
    'zithrinidi',
    'boabekimoun',
    'mantnacina',
    'kamicfeici',
    'ivenganes',
    'staphagtiostooz',
    'worvolksanca',
    'swabmicquosu',
    'highcommoucha',
    'fighheacentri',
    'heartclevnica',
    'ceiceerekul',
    'vijnofebdo',
    'inhenfeverb',
    'brousoutmino',
    'imphocogre',
    'battryvorma',
    'encatitu',
    'mercarogar',
    'inanexdean',
    'sleekefclanfeu',
    'glutaqdymil',
    'choisosale',
    'lapneucewo',
    'dingvalrudep',
    'necnawhire',
    'destnoumerly',
    'practerrero',
    'olanomvi',
    'disfdeberpo',
    'tansurpsendcam',
    'dresuncuche',
    'lecnaribna',
    'tiborguitwor',
    'geocintingprin',
    'terticora',
    'kaisserhaff',
    'kenenbek',
    'katy5452',
    'karasov',
    'katya12304',
    'kempriol',
    'kenny106',
    'katterham',
    'igor9mm',
    'borisserb',
    'leonid5',
    'viktorvor',
    'vasilek88',
    'kartcent',
    'katiish',
    'kap0ne',
    'kit9433',
    'kbozdik',
    'musikant1',
    'RussianNeuro',
    'asedag',
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  );

-- удалить связку Человека и заданных аккаунтов
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'gausithirshu',
        'ibatelzu1984',
        'belihinknusc',
        'zithrinidi',
        'boabekimoun',
        'mantnacina',
        'kamicfeici',
        'ivenganes',
        'staphagtiostooz',
        'worvolksanca',
        'swabmicquosu',
        'highcommoucha',
        'fighheacentri',
        'heartclevnica',
        'ceiceerekul',
        'vijnofebdo',
        'inhenfeverb',
        'brousoutmino',
        'imphocogre',
        'battryvorma',
        'encatitu',
        'mercarogar',
        'inanexdean',
        'sleekefclanfeu',
        'glutaqdymil',
        'choisosale',
        'lapneucewo',
        'dingvalrudep',
        'necnawhire',
        'destnoumerly',
        'practerrero',
        'olanomvi',
        'disfdeberpo',
        'tansurpsendcam',
        'dresuncuche',
        'lecnaribna',
        'tiborguitwor',
        'geocintingprin',
        'terticora',
        'kaisserhaff',
        'kenenbek',
        'katy5452',
        'karasov',
        'katya12304',
        'kempriol',
        'kenny106',
        'katterham',
        'igor9mm',
        'borisserb',
        'leonid5',
        'viktorvor',
        'vasilek88',
        'kartcent',
        'katiish',
        'kap0ne',
        'kit9433',
        'kbozdik',
        'musikant1',
        'RussianNeuro',
        'asedag',
        'mikhailfil',
        'stadevlasov',
        'aleshazhu',
        'ilyasti',
        'petrlukin',
        'petrevya',
        'fedorwm',
        'petrzimin',
        'nikitos66',
        'leonidust',
        'mikhail1un',
        'nikitada',
        'anatoliytim',
        'igorturov',
        'nikolaylow',
        'petrana',
        'mikhailempopo'
      )
      AND a.service_id =
          (
            SELECT id
            FROM service
            WHERE code = 'PIKABU_RU'
          )
  );

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

SELECT *
FROM tag_account ta
WHERE
  ta.account_id IN
  (
    SELECT id
    FROM
      account
    WHERE
      login
      IN
      (
        'gausithirshu',
        'ibatelzu1984',
        'belihinknusc',
        'zithrinidi',
        'boabekimoun',
        'mantnacina',
        'kamicfeici',
        'ivenganes',
        'staphagtiostooz',
        'worvolksanca',
        'swabmicquosu',
        'highcommoucha',
        'fighheacentri',
        'heartclevnica',
        'ceiceerekul',
        'vijnofebdo',
        'inhenfeverb',
        'brousoutmino',
        'imphocogre',
        'battryvorma',
        'encatitu',
        'mercarogar',
        'inanexdean',
        'sleekefclanfeu',
        'glutaqdymil',
        'choisosale',
        'lapneucewo',
        'dingvalrudep',
        'necnawhire',
        'destnoumerly',
        'practerrero',
        'olanomvi',
        'disfdeberpo',
        'tansurpsendcam',
        'dresuncuche',
        'lecnaribna',
        'tiborguitwor',
        'geocintingprin',
        'terticora',
        'kaisserhaff',
        'kenenbek',
        'katy5452',
        'karasov',
        'katya12304',
        'kempriol',
        'kenny106',
        'katterham',
        'igor9mm',
        'borisserb',
        'leonid5',
        'viktorvor',
        'vasilek88',
        'kartcent',
        'katiish',
        'kap0ne',
        'kit9433',
        'kbozdik',
        'musikant1',
        'RussianNeuro',
        'asedag',
        'mikhailfil',
        'stadevlasov',
        'aleshazhu',
        'ilyasti',
        'petrlukin',
        'petrevya',
        'fedorwm',
        'petrzimin',
        'nikitos66',
        'leonidust',
        'mikhail1un',
        'nikitada',
        'anatoliytim',
        'igorturov',
        'nikolaylow',
        'petrana',
        'mikhailempopo'
      )
  );

DELETE
FROM tag_account ta
WHERE
  ta.account_id IN
  (
    SELECT id
    FROM
      account
    WHERE
      login
      IN
      (
        'gausithirshu',
        'ibatelzu1984',
        'belihinknusc',
        'zithrinidi',
        'boabekimoun',
        'mantnacina',
        'kamicfeici',
        'ivenganes',
        'staphagtiostooz',
        'worvolksanca',
        'swabmicquosu',
        'highcommoucha',
        'fighheacentri',
        'heartclevnica',
        'ceiceerekul',
        'vijnofebdo',
        'inhenfeverb',
        'brousoutmino',
        'imphocogre',
        'battryvorma',
        'encatitu',
        'mercarogar',
        'inanexdean',
        'sleekefclanfeu',
        'glutaqdymil',
        'choisosale',
        'lapneucewo',
        'dingvalrudep',
        'necnawhire',
        'destnoumerly',
        'practerrero',
        'olanomvi',
        'disfdeberpo',
        'tansurpsendcam',
        'dresuncuche',
        'lecnaribna',
        'tiborguitwor',
        'geocintingprin',
        'terticora',
        'kaisserhaff',
        'kenenbek',
        'katy5452',
        'karasov',
        'katya12304',
        'kempriol',
        'kenny106',
        'katterham',
        'igor9mm',
        'borisserb',
        'leonid5',
        'viktorvor',
        'vasilek88',
        'kartcent',
        'katiish',
        'kap0ne',
        'kit9433',
        'kbozdik',
        'musikant1',
        'RussianNeuro',
        'asedag',
        'mikhailfil',
        'stadevlasov',
        'aleshazhu',
        'ilyasti',
        'petrlukin',
        'petrevya',
        'fedorwm',
        'petrzimin',
        'nikitos66',
        'leonidust',
        'mikhail1un',
        'nikitada',
        'anatoliytim',
        'igorturov',
        'nikolaylow',
        'petrana',
        'mikhailempopo'
      )
  );

DELETE FROM
  account a
WHERE
  a.login IN
  (
    'gausithirshu',
    'ibatelzu1984',
    'belihinknusc',
    'zithrinidi',
    'boabekimoun',
    'mantnacina',
    'kamicfeici',
    'ivenganes',
    'staphagtiostooz',
    'worvolksanca',
    'swabmicquosu',
    'highcommoucha',
    'fighheacentri',
    'heartclevnica',
    'ceiceerekul',
    'vijnofebdo',
    'inhenfeverb',
    'brousoutmino',
    'imphocogre',
    'battryvorma',
    'encatitu',
    'mercarogar',
    'inanexdean',
    'sleekefclanfeu',
    'glutaqdymil',
    'choisosale',
    'lapneucewo',
    'dingvalrudep',
    'necnawhire',
    'destnoumerly',
    'practerrero',
    'olanomvi',
    'disfdeberpo',
    'tansurpsendcam',
    'dresuncuche',
    'lecnaribna',
    'tiborguitwor',
    'geocintingprin',
    'terticora',
    'kaisserhaff',
    'kenenbek',
    'katy5452',
    'karasov',
    'katya12304',
    'kempriol',
    'kenny106',
    'katterham',
    'igor9mm',
    'borisserb',
    'leonid5',
    'viktorvor',
    'vasilek88',
    'kartcent',
    'katiish',
    'kap0ne',
    'kit9433',
    'kbozdik',
    'musikant1',
    'RussianNeuro',
    'asedag',
    'mikhailfil',
    'stadevlasov',
    'aleshazhu',
    'ilyasti',
    'petrlukin',
    'petrevya',
    'fedorwm',
    'petrzimin',
    'nikitos66',
    'leonidust',
    'mikhail1un',
    'nikitada',
    'anatoliytim',
    'igorturov',
    'nikolaylow',
    'petrana',
    'mikhailempopo'
  );

--vertex4te
-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'vertex4te'
  )
  AND a.service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'vertex4te'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'vertex4te'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить аккаунты
DELETE FROM account
WHERE
  login IN
  (
    'vertex4te'
  )
  AND service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );

SELECT *
FROM
  account
WHERE
  login IN
  (
    'Denhead',
    'NickOnToluca',
    'WatsonRus',
    'feuerloescher',
    'DimitarSerg',
    'Denn29',
    'dialmak',
    'pop2ROOTER',
    'franzykman',
    'redwhiterus'
  );

SELECT
  (SELECT login
   FROM
     account a
   WHERE
     a.id = ta.account_id),
  (SELECT code
   FROM
     tag t
   WHERE
     t.id = ta.tag_id
  )
FROM
  tag_account ta
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account
    WHERE
      login IN
      (
        'Denhead',
        'NickOnToluca',
        'WatsonRus',
        'feuerloescher',
        'DimitarSerg',
        'Denn29',
        'dialmak',
        'pop2ROOTER',
        'franzykman',
        'redwhiterus'
      )
  );

SELECT *
FROM
  account
WHERE
  login IN
  (
    'Denhead',
    'NickOnToluca',
    'WatsonRus',
    'feuerloescher',
    'DimitarSerg',
    'redwhiterus'
  );

SELECT
  a.login,
  (SELECT code
   FROM
     tag
   WHERE code = 'RELIABLE')
FROM
  account a
WHERE
  login IN
  (
    'Denhead',
    'NickOnToluca',
    'WatsonRus',
    'feuerloescher',
    'DimitarSerg',
    'redwhiterus'
  );

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'Denhead',
        'NickOnToluca',
        'WatsonRus',
        'feuerloescher',
        'DimitarSerg',
        'redwhiterus'
      )
)
SELECT
  acc.login,
  t.code
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

WITH acc AS
(
    SELECT
      a.login AS login,
      a.id    AS account_id
    FROM
      account a
    WHERE
      login IN
      (
        'Denhead',
        'NickOnToluca',
        'WatsonRus',
        'feuerloescher',
        'DimitarSerg',
        'redwhiterus'
      )
)
SELECT
  acc.account_id,
  t.id
FROM
  acc,
  tag t
WHERE
  t.code IN
  (
    'RELIABLE',
    'PROMO_ME'
  );

INSERT INTO tag_account (account_id, tag_id)
  WITH acc AS
  (
      SELECT
        a.login AS login,
        a.id    AS account_id
      FROM
        account a
      WHERE
        login IN
        (
          'Denhead',
          'NickOnToluca',
          'WatsonRus',
          'feuerloescher',
          'DimitarSerg',
          'redwhiterus'
        )
  )
  SELECT
    acc.account_id,
    t.id
  FROM
    acc,
    tag t
  WHERE
    t.code IN
    (
      'RELIABLE',
      'PROMO_ME'
    );

SELECT
  to_char(row_number()
          OVER (), '09') ||
  ' http://'
  || -- proxy_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ':'
  || -- proxy_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || '@'
  || -- proxy_address
  (SELECT s.code
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
     JOIN tag_service ts ON s.id = ts.service_id
     JOIN tag t ON ts.tag_id = t.id
   WHERE ps.id = p.id AND t.code = 'PROXY')
  || ' '
  || -- account_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- account_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  || ' '
  || -- mail_login
  (SELECT sa.login
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- mail_password
  (SELECT sa.password
   FROM person ps
     JOIN person_account psa ON ps.id = psa.person_id
     JOIN account sa ON psa.account_id = sa.id
     JOIN service s ON sa.service_id = s.id
   WHERE ps.id = p.id AND s.code = 'MAIL_RU')
  || ' '
  || -- account tag
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  )
    AS person_account,
  (
    SELECT t.code
    FROM person ps
      JOIN person_account psa ON ps.id = psa.person_id
      JOIN account sa ON psa.account_id = sa.id
      JOIN tag_account tsa
        ON sa.id = tsa.account_id
      JOIN tag t
        ON tsa.tag_id = t.id
    WHERE ps.id = p.id AND
          (
            t.code = 'BLOCK'
            OR t.code = 'RELIABLE'
          )
  ) AS account_tag
FROM
  person p
WHERE
  EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
        JOIN tag_service ts ON s.id = ts.service_id
        JOIN tag t ON ts.tag_id = t.id
      WHERE ps.id = p.id AND t.code = 'PROXY'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'MAIL_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM person ps
        JOIN person_account psa ON ps.id = psa.person_id
        JOIN account sa ON psa.account_id = sa.id
        JOIN service s ON sa.service_id = s.id
      WHERE ps.id = p.id AND s.code = 'PIKABU_RU'
  )
  AND EXISTS(
      SELECT NULL
      FROM
        account a
        JOIN tag_account ta
          ON a.id = ta.account_id
        JOIN tag t
          ON ta.tag_id = t.id
        JOIN person_account pa
          ON a.id = pa.account_id
        JOIN person pe
          ON pa.person_id = pe.id
      WHERE
        pe.id = p.id
        AND t.code = 'RELIABLE'
        AND a.service_id =
            (
              SELECT id
              FROM service
              WHERE code = 'PIKABU_RU'
            )
        AND a.is_hidden = FALSE
  )
ORDER BY account_tag DESC;

-- выбрать аккаунты для удаления
SELECT *
FROM
  account a
WHERE
  a.login IN
  (
    'feuerloescher',
    'DimitarSerg',
    'NickOnToluca',
    'redwhiterus'
  )
  AND a.service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов и Персон
DELETE FROM person_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'feuerloescher',
        'DimitarSerg',
        'NickOnToluca',
        'redwhiterus'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить теги аккаунтов
DELETE FROM tag_account
WHERE
  account_id IN
  (
    SELECT id
    FROM
      account a
    WHERE
      a.login IN
      (
        'feuerloescher',
        'DimitarSerg',
        'NickOnToluca',
        'redwhiterus'
      )
      AND a.service_id =
          (
            SELECT id
            FROM
              service
            WHERE
              code = 'PIKABU_RU'
          )
  );

-- удалить аккаунты
DELETE FROM account
WHERE
  login IN
  (
    'feuerloescher',
    'DimitarSerg',
    'NickOnToluca',
    'redwhiterus'
  )
  AND service_id =
      (
        SELECT id
        FROM
          service
        WHERE
          code = 'PIKABU_RU'
      );

-- удалить связку аккаунтов с Персонами у которых нет профилей на Пикабу
DELETE FROM person_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_account psa
        WHERE
          NOT exists
          (
              SELECT NULL
              FROM
                person_account psae
                JOIN account sae
                  ON psae.account_id = sae.id
                JOIN service se
                  ON sae.service_id = se.id
              WHERE
                psae.person_id = psa.person_id
                AND se.code = 'PIKABU_RU'
          )
      );

-- удалить Персон у которых нет связок с аккаунтами
DELETE FROM person
WHERE id IN
      (
        SELECT p.id
        FROM
          person p
          LEFT JOIN person_account psa
            ON p.id = psa.person_id
        WHERE
          psa.id IS NULL
      );
