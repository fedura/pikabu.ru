SELECT
  a.login    AS login,
  a.password AS password,
  s.code     AS proxy
FROM
  tag t
  JOIN tag_service ts
    ON t.id = ts.tag_id
  JOIN service s
    ON ts.service_id = s.id
  JOIN account a
    ON s.id = a.service_id
  LEFT JOIN person_account pa
    ON a.id = pa.account_id
WHERE
  t.code = 'PROXY'
  AND pa.id IS NULL;
;

DELETE FROM post;

INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'q', 'й');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'w', 'ц');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'e', 'у');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'r', 'к');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 't', 'е');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'y', 'н');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'ш
щ
з рбло', 'г');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'i', 'ш
щ
з рбло');
INSERT INTO public.post (is_hidden, title, body) VALUES (TRUE, 'o', 'ф');
INSERT INTO public.post (is_hidden, title, body) VALUES (FALSE, 'p', 'ы');
INSERT INTO public.post (is_hidden, title, body) VALUES (FALSE, 'a', 'в');
INSERT INTO public.post (is_hidden, title, body) VALUES (FALSE, 's', 'а');

SELECT
  a.login AS login,
  a.id    AS id
FROM
  tag t
  JOIN tag_account ta
    ON t.id = ta.tag_id
  JOIN account a
    ON ta.account_id = a.id
WHERE
  t.code = 'RELIABLE';

SELECT
  p.id    AS post_id,
  p.title AS post_title,
  p.body  AS post_body
FROM
  post p
  LEFT JOIN account_post ap
    ON p.id = ap.post_id
WHERE
  p.is_hidden IS TRUE
  AND ap.id IS NULL;

SELECT
  *
FROM
  account_post ap
  JOIN account a
    ON ap.account_id = a.id
  JOIN post p
    ON ap.post_id = p.id
;

TRUNCATE TABLE account_post;

    SELECT
      (SELECT code FROM tag WHERE code = 'PROMO_ME'),
      a.login
FROM
  account a
WHERE
  a.login in
  (
    'staphagtiostooz',
    'sleekefclanfeu',
    'inanexdean',
    'tiborguitwor',
    'vijnofebdo',
    'dingvalrudep',
    'necnawhire',
    'destnoumerly',
    'heartclevnica',
    'mercarogar',
    'kamicfeici',
    'rarecrepa',
    'glutaqdymil',
    'kiriknik',
    'terticora',
    'ocelabex',
    'zithrinidi',
    'gausithirshu',
    'mantnacina',
    'battryvorma',
    'fighheacentri',
    'brousoutmino',
    'swabmicquosu',
    'kivido',
    'encatitu',
    'imphocogre',
    'ivenganes',
    'ceiceerekul',
    'highcommoucha',
    'inhenfeverb',
    'lapneucewo',
    'worvolksanca',
    'lecnaribna',
    'geocintingprin',
    'belihinknusc',
    'winddustselfle',
    'abipsdatun',
    'practerrero',
    'tiobalabest',
    'olanomvi',
    'boabekimoun',
    'ibatelzu1984'
  )
;

    SELECT
      (SELECT id FROM tag WHERE code = 'PROMO_ME'),
      a.id
FROM
  account a
WHERE
  a.login in
  (
    'staphagtiostooz',
    'sleekefclanfeu',
    'inanexdean',
    'tiborguitwor',
    'vijnofebdo',
    'dingvalrudep',
    'necnawhire',
    'destnoumerly',
    'heartclevnica',
    'mercarogar',
    'kamicfeici',
    'rarecrepa',
    'glutaqdymil',
    'kiriknik',
    'terticora',
    'ocelabex',
    'zithrinidi',
    'gausithirshu',
    'mantnacina',
    'battryvorma',
    'fighheacentri',
    'brousoutmino',
    'swabmicquosu',
    'kivido',
    'encatitu',
    'imphocogre',
    'ivenganes',
    'ceiceerekul',
    'highcommoucha',
    'inhenfeverb',
    'lapneucewo',
    'worvolksanca',
    'lecnaribna',
    'geocintingprin',
    'belihinknusc',
    'winddustselfle',
    'abipsdatun',
    'practerrero',
    'tiobalabest',
    'olanomvi',
    'boabekimoun',
    'ibatelzu1984'
  )
;

INSERT INTO tag_account (tag_id, account_id)
    SELECT
      (SELECT id FROM tag WHERE code = 'PROMO_ME'),
      a.id
FROM
  account a
WHERE
  a.login in
  (
    'staphagtiostooz',
    'sleekefclanfeu',
    'inanexdean',
    'tiborguitwor',
    'vijnofebdo',
    'dingvalrudep',
    'necnawhire',
    'destnoumerly',
    'heartclevnica',
    'mercarogar',
    'kamicfeici',
    'rarecrepa',
    'glutaqdymil',
    'kiriknik',
    'terticora',
    'ocelabex',
    'zithrinidi',
    'gausithirshu',
    'mantnacina',
    'battryvorma',
    'fighheacentri',
    'brousoutmino',
    'swabmicquosu',
    'kivido',
    'encatitu',
    'imphocogre',
    'ivenganes',
    'ceiceerekul',
    'highcommoucha',
    'inhenfeverb',
    'lapneucewo',
    'worvolksanca',
    'lecnaribna',
    'geocintingprin',
    'belihinknusc',
    'winddustselfle',
    'abipsdatun',
    'practerrero',
    'tiobalabest',
    'olanomvi',
    'boabekimoun',
    'ibatelzu1984'
  )
;

SELECT
  a.login AS login,
  a.id    AS id
FROM
  account a
WHERE
  EXISTS(
    SELECT NULL FROM tag t join tag_account ta on t.id = ta.tag_id WHERE ta.account_id = a.id and t.code = 'RELIABLE'
  )
AND
  EXISTS(
    SELECT NULL FROM tag t join tag_account ta on t.id = ta.tag_id WHERE ta.account_id = a.id and t.code = 'PROMO_ME'
  )
;