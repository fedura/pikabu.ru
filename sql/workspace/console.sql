DROP TABLE IF EXISTS tag_service;
DROP TABLE IF EXISTS tag_service_account;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS person_service_account;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS service_account;
DROP TABLE IF EXISTS service;

CREATE TABLE service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_code
  ON service (code);

CREATE TABLE service_account
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service_account
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  service_id  INTEGER
    CONSTRAINT fk_service_account_service_id
    REFERENCES service (id),
  login       TEXT,
  password    TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_account_service_id_login
  ON service_account (service_id, login);

CREATE TABLE person
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_person
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_person_code
  ON person (code);

CREATE TABLE person_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_person_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  person_id          INTEGER
    CONSTRAINT fk_person_service_account_person_id
    REFERENCES person (id),
  service_account_id INTEGER
    CONSTRAINT fk_person_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE UNIQUE INDEX ux_person_service_account_service_account_id
  ON person_service_account (service_account_id);
CREATE INDEX ix_person_service_account_person_id_service_account_id
  ON person_service_account (person_id, service_account_id);

CREATE TABLE tag
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_tag_code
  ON tag (code);

CREATE TABLE tag_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_tag_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  tag_id             INTEGER
    CONSTRAINT fk_tag_service_account_tag_id
    REFERENCES tag (id),
  service_account_id INTEGER
    CONSTRAINT fk_tag_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE INDEX ix_tag_service_account_tag_id_service_account_id
  ON tag_service_account (tag_id, service_account_id);

CREATE TABLE tag_service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  tag_id      INTEGER
    CONSTRAINT fk_tag_service_tag_id
    REFERENCES tag (id),
  service_id  INTEGER
    CONSTRAINT fk_tag_service_service_id
    REFERENCES service (id)
);
CREATE UNIQUE INDEX ix_tag_service_tag_id_service_id
  ON tag_service (tag_id, service_id);

-- RELIABLE
SELECT 'http://' || proxy_login || ':' || proxy_password || '@' || proxy_address || ' ' || account_login || ' ' ||
       account_password
FROM
  (
    WITH RECURSIVE r AS (
      SELECT 0 AS i

      UNION

      SELECT i + 1 AS i
      FROM r
      WHERE i < -1 + (SELECT count(*)
                      FROM service s
                      WHERE exists(SELECT NULL
                                   FROM tag_service ts
                                   WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                               FROM tag
                                                                               WHERE code = 'PROXY')))
    )
    SELECT
      r.i       AS number,

      (SELECT (SELECT login
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_login,
      (SELECT (SELECT password
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_password,

      (SELECT code
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_address,
      (SELECT sa.login
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_login,
      (SELECT sa.password
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_password
    FROM
      r
    ORDER BY proxy_address
  ) T;

-- RELIABLE
SELECT account_login
FROM
  (
    WITH RECURSIVE r AS (
      SELECT 0 AS i

      UNION

      SELECT i + 1 AS i
      FROM r
      WHERE i < -1 + (SELECT count(*)
                      FROM service s
                      WHERE exists(SELECT NULL
                                   FROM tag_service ts
                                   WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                               FROM tag
                                                                               WHERE code = 'PROXY')))
    )
    SELECT
      (SELECT code
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_address,
      (SELECT sa.login
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_login
    FROM
      r
    ORDER BY proxy_address
  ) T;

-- RELIABLE
INSERT INTO person (code)
  SELECT account_login
  FROM
    (
      WITH RECURSIVE r AS (
        SELECT 0 AS i

        UNION

        SELECT i + 1 AS i
        FROM r
        WHERE i < -1 + (SELECT count(*)
                        FROM service s
                        WHERE exists(SELECT NULL
                                     FROM tag_service ts
                                     WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                                 FROM tag
                                                                                 WHERE code = 'PROXY')))
      )
      SELECT
        (SELECT code
         FROM service s
         WHERE exists(SELECT NULL
                      FROM tag_service ts
                      WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                  FROM tag
                                                                  WHERE code = 'PROXY'))
         OFFSET r.i
         LIMIT 1) AS proxy_address,
        (SELECT sa.login
         FROM service_account sa
           JOIN tag_service_account tsa
             ON sa.id = tsa.service_account_id
           JOIN tag t
             ON tsa.tag_id = t.id
         WHERE sa.service_id = (SELECT id
                                FROM service
                                WHERE code = 'PIKABU_RU')
               AND t.code = 'RELIABLE'
         OFFSET r.i
         LIMIT 1) AS account_login
      FROM
        r
      ORDER BY proxy_address
    ) T;

SELECT
  p.code,
  (SELECT sa.login
   FROM service_account sa
   WHERE sa.login = p.code)
FROM person p;

INSERT INTO person_service_account (person_id, service_account_id)
  SELECT
    p.id,
    (SELECT sa.id
     FROM service_account sa
     WHERE sa.login = p.code)
  FROM person p;


SELECT
  account_login,
  (SELECT code
   FROM service s
   WHERE s.id = proxy_address_id) AS proxy_address,
  (SELECT sa.id
   FROM service_account sa
   WHERE sa.service_id = proxy_address_id)
FROM
  (
    WITH RECURSIVE r AS (
      SELECT 0 AS i

      UNION

      SELECT i + 1 AS i
      FROM r
      WHERE i < -1 + (SELECT count(*)
                      FROM service s
                      WHERE exists(SELECT NULL
                                   FROM tag_service ts
                                   WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                               FROM tag
                                                                               WHERE code = 'PROXY')))
    )
    SELECT
      (SELECT id
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_address_id,
      (SELECT sa.login
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_login
    FROM
      r
    ORDER BY proxy_address_id
  ) T;


SELECT
  (SELECT p.id
   FROM person p
   WHERE p.code = account_login),
  proxy_account_id
FROM
  (
    SELECT
      account_login,
      (SELECT code
       FROM service s
       WHERE s.id = proxy_address_id)          AS proxy_address,
      (SELECT sa.id
       FROM service_account sa
       WHERE sa.service_id = proxy_address_id) AS proxy_account_id
    FROM
      (
        WITH RECURSIVE r AS (
          SELECT 0 AS i

          UNION

          SELECT i + 1 AS i
          FROM r
          WHERE i < -1 + (SELECT count(*)
                          FROM service s
                          WHERE exists(SELECT NULL
                                       FROM tag_service ts
                                       WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                                   FROM tag
                                                                                   WHERE code = 'PROXY')))
        )
        SELECT
          (SELECT id
           FROM service s
           WHERE exists(SELECT NULL
                        FROM tag_service ts
                        WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                    FROM tag
                                                                    WHERE code = 'PROXY'))
           OFFSET r.i
           LIMIT 1) AS proxy_address_id,
          (SELECT sa.login
           FROM service_account sa
             JOIN tag_service_account tsa
               ON sa.id = tsa.service_account_id
             JOIN tag t
               ON tsa.tag_id = t.id
           WHERE sa.service_id = (SELECT id
                                  FROM service
                                  WHERE code = 'PIKABU_RU')
                 AND t.code = 'RELIABLE'
           OFFSET r.i
           LIMIT 1) AS account_login
        FROM
          r
        ORDER BY proxy_address_id
      ) T
  ) A;

INSERT INTO person_service_account (person_id, service_account_id)
  SELECT
    (SELECT p.id
     FROM person p
     WHERE p.code = account_login),
    proxy_account_id
  FROM
    (
      SELECT
        account_login,
        (SELECT sa.id
         FROM service_account sa
         WHERE sa.service_id = proxy_address_id) AS proxy_account_id
      FROM
        (
          WITH RECURSIVE r AS (
            SELECT 0 AS i

            UNION

            SELECT i + 1 AS i
            FROM r
            WHERE i < -1 + (SELECT count(*)
                            FROM service s
                            WHERE exists(SELECT NULL
                                         FROM tag_service ts
                                         WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                                     FROM tag
                                                                                     WHERE code = 'PROXY')))
          )
          SELECT
            (SELECT id
             FROM service s
             WHERE exists(SELECT NULL
                          FROM tag_service ts
                          WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                      FROM tag
                                                                      WHERE code = 'PROXY'))
             OFFSET r.i
             LIMIT 1) AS proxy_address_id,
            (SELECT sa.login
             FROM service_account sa
               JOIN tag_service_account tsa
                 ON sa.id = tsa.service_account_id
               JOIN tag t
                 ON tsa.tag_id = t.id
             WHERE sa.service_id = (SELECT id
                                    FROM service
                                    WHERE code = 'PIKABU_RU')
                   AND t.code = 'RELIABLE'
             OFFSET r.i
             LIMIT 1) AS account_login
          FROM
            r
          ORDER BY proxy_address_id
        ) T
    ) A;

SELECT
  p.code   AS person,
  sa.login AS account,
  s.code   AS service
FROM person p
  JOIN person_service_account psa ON p.id = psa.person_id
  JOIN service_account sa ON psa.service_account_id = sa.id
  JOIN service s ON sa.service_id = s.id
ORDER BY p.code;

SELECT COUNT(*)
FROM service_account sa
  JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  JOIN tag t
    ON tsa.tag_id = t.id
WHERE sa.service_id = (SELECT id
                       FROM service
                       WHERE code = 'PIKABU_RU')
      AND t.code = 'RELIABLE';

SELECT COUNT(*)
FROM service_account sa
WHERE sa.service_id = (SELECT id
                       FROM service
                       WHERE code = 'PIKABU_RU')
      AND NOT EXISTS(SELECT NULL
                     FROM tag_service_account tsa
                       JOIN tag t ON tsa.tag_id = t.id
                     WHERE t.code = 'RELIABLE' AND tsa.service_account_id = sa.id);

SELECT (SELECT p.code
        FROM person p
        WHERE p.id = psa.person_id)
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service_account tsa
    ON sa.id = tsa.service_account_id
  JOIN tag t ON
               tsa.tag_id = t.id
WHERE
  EXISTS
  (
      SELECT NULL
      FROM
        person_service_account psae
        JOIN service_account sa
          ON psae.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        psae.person_id = psa.person_id
        AND t.code = 'PROXY'
  )
  AND psa.person_id = psa.person_id
  AND s.code = 'PIKABU_RU'
  AND t.code = 'FAIL';

SELECT
  (SELECT p.code
   FROM person p
   WHERE p.id = psa.person_id),
  (SELECT ssa.login
   FROM service_account ssa
   WHERE ssa.id = psa.service_account_id)
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id

WHERE
  EXISTS
  (
      SELECT NULL
      FROM
        person_service_account psae
        JOIN service_account sa
          ON psae.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service_account tsa
          ON sa.id = tsa.service_account_id
        JOIN tag t ON
                     tsa.tag_id = t.id
      WHERE
        psae.person_id = psa.person_id
        AND s.code = 'PIKABU_RU'
        AND t.code = 'FAIL'
  )
  AND t.code = 'PROXY';


DELETE FROM person_service_account
WHERE id IN
      (
        SELECT psa.id
        FROM
          person_service_account psa
          JOIN service_account sa
            ON psa.service_account_id = sa.id
          JOIN service s
            ON sa.service_id = s.id
          JOIN tag_service ts
            ON s.id = ts.service_id
          JOIN tag t
            ON ts.tag_id = t.id

        WHERE
          EXISTS
          (
              SELECT NULL
              FROM
                person_service_account psae
                JOIN service_account sa
                  ON psae.service_account_id = sa.id
                JOIN service s
                  ON sa.service_id = s.id
                JOIN tag_service_account tsa
                  ON sa.id = tsa.service_account_id
                JOIN tag t ON
                             tsa.tag_id = t.id
              WHERE
                psae.person_id = psa.person_id
                AND s.code = 'PIKABU_RU'
                AND t.code = 'FAIL'
          )
          AND t.code = 'PROXY'
      );

SELECT COUNT(*)
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id
WHERE
  t.code = 'PROXY'
  AND NOT EXISTS
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT (SELECT sa.login
        FROM service_account sa
          JOIN tag_service_account tsa
            ON sa.id = tsa.service_account_id
          JOIN tag t
            ON tsa.tag_id = t.id
        WHERE sa.service_id = (SELECT id
                               FROM service
                               WHERE code = 'PIKABU_RU')
              AND t.code = 'RELIABLE'
              AND NOT EXISTS(SELECT NULL
                             FROM person_service_account psa
                             WHERE psa.service_account_id = sa.id)
        OFFSET r.i
        LIMIT 1) AS account_login
FROM
  r;

INSERT INTO person (code)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT (SELECT sa.login
          FROM service_account sa
            JOIN tag_service_account tsa
              ON sa.id = tsa.service_account_id
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE sa.service_id = (SELECT id
                                 FROM service
                                 WHERE code = 'PIKABU_RU')
                AND t.code = 'RELIABLE'
                AND NOT EXISTS(SELECT NULL
                               FROM person_service_account psa
                               WHERE psa.service_account_id = sa.id)
          OFFSET r.i
          LIMIT 1) AS account_login
  FROM
    r;

SELECT
  p.id,
  (
    SELECT sa.id
    FROM service_account sa
    WHERE sa.login = p.code
  )
FROM
  person p
WHERE
  NOT EXISTS(
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.person_id = p.id
  );

INSERT INTO person_service_account (person_id, service_account_id)
  SELECT
    p.id,
    (
      SELECT sa.id
      FROM service_account sa
      WHERE sa.login = p.code
    )
  FROM
    person p
  WHERE
    NOT EXISTS(
        SELECT NULL
        FROM
          person_service_account psa
        WHERE
          psa.person_id = p.id
    );

SELECT psa.service_account_id
FROM
  person_service_account psa
  JOIN service_account sa
    ON psa.service_account_id = sa.id
  JOIN service s
    ON sa.service_id = s.id
WHERE
  s.code = 'PIKABU_RU'
  AND NOT EXISTS(
      SELECT NULL
      FROM
        tag_service_account tsa
        JOIN tag t
          ON tsa.tag_id = t.id
      WHERE
        tsa.service_account_id = sa.id
        AND t.code = 'FAIL'
  )
  AND NOT EXISTS
  (
      SELECT NULL
      FROM
        person pe
        JOIN person_service_account psae
          ON pe.id = psae.person_id
        JOIN service_account sae
          ON psae.service_account_id = sae.id
        JOIN service se
          ON sae.service_id = se.id
        JOIN tag_service tse
          ON se.id = tse.service_id
        JOIN tag te
          ON tse.tag_id = te.id
      WHERE
        te.code = 'PROXY'
        AND pe.id = psa.person_id
  );

WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      service_account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_service_account psa
          WHERE
            psa.service_account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.service_account_id
    FROM
      person_service_account psa
      JOIN service_account sa
        ON psa.service_account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_service_account tsa
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE
            tsa.service_account_id = sa.id
            AND t.code = 'FAIL'
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_service_account psae
              ON pe.id = psae.person_id
            JOIN service_account sae
              ON psae.service_account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS pikabu_account
FROM
  r;


WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT COUNT(*)
                  FROM
                    service_account sa
                    JOIN service s
                      ON sa.service_id = s.id
                    JOIN tag_service ts
                      ON s.id = ts.service_id
                    JOIN tag t
                      ON ts.tag_id = t.id
                  WHERE
                    t.code = 'PROXY'
                    AND NOT EXISTS
                    (
                        SELECT NULL
                        FROM
                          person_service_account psa
                        WHERE
                          psa.service_account_id = sa.id
                    ))
)
SELECT
  (
    SELECT sa.id
    FROM
      service_account sa
      JOIN service s
        ON sa.service_id = s.id
      JOIN tag_service ts
        ON s.id = ts.service_id
      JOIN tag t
        ON ts.tag_id = t.id
    WHERE
      t.code = 'PROXY'
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person_service_account psa
          WHERE
            psa.service_account_id = sa.id
      )
    OFFSET r.i
    LIMIT 1) AS proxy_account,
  (
    SELECT psa.person_id
    FROM
      person_service_account psa
      JOIN service_account sa
        ON psa.service_account_id = sa.id
      JOIN service s
        ON sa.service_id = s.id
    WHERE
      s.code = 'PIKABU_RU'
      AND NOT EXISTS(
          SELECT NULL
          FROM
            tag_service_account tsa
            JOIN tag t
              ON tsa.tag_id = t.id
          WHERE
            tsa.service_account_id = sa.id
            AND t.code = 'FAIL'
      )
      AND NOT EXISTS
      (
          SELECT NULL
          FROM
            person pe
            JOIN person_service_account psae
              ON pe.id = psae.person_id
            JOIN service_account sae
              ON psae.service_account_id = sae.id
            JOIN service se
              ON sae.service_id = se.id
            JOIN tag_service tse
              ON se.id = tse.service_id
            JOIN tag te
              ON tse.tag_id = te.id
          WHERE
            te.code = 'PROXY'
            AND pe.id = psa.person_id
      )
    OFFSET r.i
    LIMIT 1) AS person
FROM
  r;

INSERT INTO person_service_account (service_account_id, person_id)
  WITH RECURSIVE r AS (
    SELECT 0 AS i

    UNION

    SELECT i + 1 AS i
    FROM r
    WHERE i < -1 + (SELECT COUNT(*)
                    FROM
                      service_account sa
                      JOIN service s
                        ON sa.service_id = s.id
                      JOIN tag_service ts
                        ON s.id = ts.service_id
                      JOIN tag t
                        ON ts.tag_id = t.id
                    WHERE
                      t.code = 'PROXY'
                      AND NOT EXISTS
                      (
                          SELECT NULL
                          FROM
                            person_service_account psa
                          WHERE
                            psa.service_account_id = sa.id
                      ))
  )
  SELECT
    (
      SELECT sa.id
      FROM
        service_account sa
        JOIN service s
          ON sa.service_id = s.id
        JOIN tag_service ts
          ON s.id = ts.service_id
        JOIN tag t
          ON ts.tag_id = t.id
      WHERE
        t.code = 'PROXY'
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person_service_account psa
            WHERE
              psa.service_account_id = sa.id
        )
      OFFSET r.i
      LIMIT 1) AS proxy_account,
    (
      SELECT psa.person_id
      FROM
        person_service_account psa
        JOIN service_account sa
          ON psa.service_account_id = sa.id
        JOIN service s
          ON sa.service_id = s.id
      WHERE
        s.code = 'PIKABU_RU'
        AND NOT EXISTS(
            SELECT NULL
            FROM
              tag_service_account tsa
              JOIN tag t
                ON tsa.tag_id = t.id
            WHERE
              tsa.service_account_id = sa.id
              AND t.code = 'FAIL'
        )
        AND NOT EXISTS
        (
            SELECT NULL
            FROM
              person pe
              JOIN person_service_account psae
                ON pe.id = psae.person_id
              JOIN service_account sae
                ON psae.service_account_id = sae.id
              JOIN service se
                ON sae.service_id = se.id
              JOIN tag_service tse
                ON se.id = tse.service_id
              JOIN tag te
                ON tse.tag_id = te.id
            WHERE
              te.code = 'PROXY'
              AND pe.id = psa.person_id
        )
      OFFSET r.i
      LIMIT 1) AS person
  FROM
    r;


SELECT 'http://'
       || -- proxy_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ':'
       || -- proxy_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || '@'
       || -- proxy_address
       (SELECT s.code
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY')
       || ' '
       || -- account_login
       (SELECT sa.login
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
       || ' '
       || -- account_password
       (SELECT sa.password
        FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id

        WHERE ps.id = p.id AND s.code = 'PIKABU_RU')
  AS proxy_login_password
FROM
  person p
WHERE
  EXISTS(
    SELECT NULL FROM person ps
          JOIN person_service_account psa ON ps.id = psa.person_id
          JOIN service_account sa ON psa.service_account_id = sa.id
          JOIN service s ON sa.service_id = s.id
          JOIN tag_service ts ON s.id = ts.service_id
          JOIN tag t ON ts.tag_id = t.id
        WHERE ps.id = p.id AND t.code = 'PROXY'
  )
ORDER BY proxy_login_password;

SELECT *
FROM person p
  JOIN person_service_account psa ON p.id = psa.person_id
  JOIN service_account sa ON psa.service_account_id = sa.id
  JOIN service s ON sa.service_id = s.id
left join tag_service ts on s.id = ts.service_id
  left join tag tts on ts.tag_id = tts.id
left join tag_service_account tsa on sa.id = tsa.service_account_id
left join tag tsat on tsa.tag_id = tsat.id;

