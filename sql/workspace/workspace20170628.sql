SELECT *
FROM
  account a
WHERE
  a.login = 'kirl0g';

SELECT (
  SELECT code
  FROM
    tag
  WHERE
    id = ta.tag_id
)
FROM
  tag_account ta
WHERE
  ta.account_id =
  (
    SELECT id
    FROM
      account
    WHERE
      login = 'kirl0g'
  );

SELECT *
FROM
  tag_account ta
WHERE
  ta.tag_id =
  (
    SELECT id
    FROM
      tag
    WHERE
      code = 'PROMO_ME'
  )
  AND
  ta.account_id =
  (
    SELECT id
    FROM
      account
    WHERE
      login = 'kirl0g'
  );

DELETE FROM tag_account
WHERE id in (SELECT id
FROM
  tag_account ta
WHERE
  ta.tag_id =
  (
    SELECT id
    FROM
      tag
    WHERE
      code = 'PROMO_ME'
  )
  AND
  ta.account_id =
  (
    SELECT id
    FROM
      account
    WHERE
      login = 'kirl0g'
  ))
;


SELECT
  (select login from account where id = account_post.account_id)
FROM
  account_post
WHERE
  post_id = 113
;

SELECT
  *
FROM
  account_post
WHERE
  account_id =
  (
    SELECT
    id
    FROM
      account
    WHERE
      login = 'vertex4te'
  );

UPDATE post set is_hidden = true WHERE id = 110;

DELETE
FROM
  account_post
WHERE
  account_id =
  (
    SELECT
    id
    FROM
      account
    WHERE
      login = 'vertex4te'
  );