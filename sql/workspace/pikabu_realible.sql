DROP TABLE IF EXISTS tag_service;
DROP TABLE IF EXISTS tag_service_account;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS person_service_account;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS service_account;
DROP TABLE IF EXISTS service;

CREATE TABLE service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_code
  ON service (code);

CREATE TABLE service_account
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service_account
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  service_id  INTEGER
    CONSTRAINT fk_service_account_service_id
    REFERENCES service (id),
  login       TEXT,
  password    TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_account_service_id_login
  ON service_account (service_id, login);

CREATE TABLE person
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_person
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_person_code
  ON person (code);

CREATE TABLE person_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_person_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  person_id          INTEGER
    CONSTRAINT fk_person_service_account_person_id
    REFERENCES person (id),
  service_account_id INTEGER
    CONSTRAINT fk_person_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE UNIQUE INDEX ux_person_service_account_service_account_id
  ON person_service_account (service_account_id);
CREATE INDEX ix_person_service_account_person_id_service_account_id
  ON person_service_account (person_id, service_account_id);

CREATE TABLE tag
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_tag_code
  ON tag (code);

CREATE TABLE tag_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_tag_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  tag_id             INTEGER
    CONSTRAINT fk_tag_service_account_tag_id
    REFERENCES tag (id),
  service_account_id INTEGER
    CONSTRAINT fk_tag_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE INDEX ix_tag_service_account_tag_id_service_account_id
  ON tag_service_account (tag_id, service_account_id);

CREATE TABLE tag_service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  tag_id      INTEGER
    CONSTRAINT fk_tag_service_tag_id
    REFERENCES tag (id),
  service_id  INTEGER
    CONSTRAINT fk_tag_service_service_id
    REFERENCES service (id)
);
CREATE UNIQUE INDEX ix_tag_service_tag_id_service_id
  ON tag_service (tag_id, service_id);

SELECT *
FROM service
OFFSET 20
LIMIT 1;


WITH RECURSIVE r AS (
  SELECT 0 AS i

  UNION

  SELECT i + 1 AS i
  FROM r
  WHERE i < -1 + (SELECT count(*)
                  FROM service s
                  WHERE exists(SELECT NULL
                               FROM tag_service ts
                               WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                           FROM tag
                                                                           WHERE code = 'PROXY')))
)
SELECT
  r.i       AS number,

  (SELECT (SELECT login
           FROM service_account sa
           WHERE sa.service_id = s.id
           LIMIT 1)
   FROM service s
   WHERE exists(SELECT NULL
                FROM tag_service ts
                WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                            FROM tag
                                                            WHERE code = 'PROXY'))
   OFFSET r.i
   LIMIT 1) AS proxy_login,
  (SELECT (SELECT password
           FROM service_account sa
           WHERE sa.service_id = s.id
           LIMIT 1)
   FROM service s
   WHERE exists(SELECT NULL
                FROM tag_service ts
                WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                            FROM tag
                                                            WHERE code = 'PROXY'))
   OFFSET r.i
   LIMIT 1) AS proxy_password,

  (SELECT code
   FROM service s
   WHERE exists(SELECT NULL
                FROM tag_service ts
                WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                            FROM tag
                                                            WHERE code = 'PROXY'))
   OFFSET r.i
   LIMIT 1) AS proxy_address,
  (SELECT sa.login
   FROM service_account sa
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
   OFFSET r.i
   LIMIT 1) AS account_login,
  (SELECT sa.password
   FROM service_account sa
   WHERE sa.service_id = (SELECT id
                          FROM service
                          WHERE code = 'PIKABU_RU')
   OFFSET r.i
   LIMIT 1) AS account_password
FROM
  r
ORDER BY proxy_address;
--ORDER BY number, proxy_address;


SELECT code
FROM service s
WHERE exists(SELECT NULL
             FROM tag_service ts
             WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                         FROM tag
                                                         WHERE code = 'PROXY'));

-- #http://username:password@ip:port
-- RELIABLE
SELECT 'http://' || proxy_login || ':' || proxy_password || '@' || proxy_address || ' ' || account_login || ' ' ||
       account_password
FROM
  (


    WITH RECURSIVE r AS (
      SELECT 0 AS i

      UNION

      SELECT i + 1 AS i
      FROM r
      WHERE i < -1 + (SELECT count(*)
                      FROM service s
                      WHERE exists(SELECT NULL
                                   FROM tag_service ts
                                   WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                               FROM tag
                                                                               WHERE code = 'PROXY')))
    )
    SELECT
      r.i       AS number,

      (SELECT (SELECT login
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_login,
      (SELECT (SELECT password
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_password,

      (SELECT code
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_address,
      (SELECT sa.login
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_login,
      (SELECT sa.password
       FROM service_account sa
         JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
         JOIN tag t
           ON tsa.tag_id = t.id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND t.code = 'RELIABLE'
       OFFSET r.i
       LIMIT 1) AS account_password
    FROM
      r
    ORDER BY proxy_address
  ) T;


-- NOT
SELECT 'http://' || proxy_login || ':' || proxy_password || '@' || proxy_address || ' ' || account_login || ' ' ||
       account_password
FROM
  (


    WITH RECURSIVE r AS (
      SELECT 0 AS i

      UNION

      SELECT i + 1 AS i
      FROM r
      WHERE i < -1 + (SELECT count(*)
                      FROM service s
                      WHERE exists(SELECT NULL
                                   FROM tag_service ts
                                   WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                               FROM tag
                                                                               WHERE code = 'PROXY')))
    )
    SELECT
      r.i       AS number,

      (SELECT (SELECT login
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_login,
      (SELECT (SELECT password
               FROM service_account sa
               WHERE sa.service_id = s.id
               LIMIT 1)
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_password,

      (SELECT code
       FROM service s
       WHERE exists(SELECT NULL
                    FROM tag_service ts
                    WHERE ts.service_id = s.id AND ts.tag_id = (SELECT id
                                                                FROM tag
                                                                WHERE code = 'PROXY'))
       OFFSET r.i
       LIMIT 1) AS proxy_address,
      (SELECT sa.login
       FROM service_account sa
         LEFT JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND tsa.id IS NULL
       OFFSET r.i
       LIMIT 1) AS account_login,
      (SELECT sa.password
       FROM service_account sa
         LEFT JOIN tag_service_account tsa
           ON sa.id = tsa.service_account_id
       WHERE sa.service_id = (SELECT id
                              FROM service
                              WHERE code = 'PIKABU_RU')
             AND tsa.id IS NULL
       OFFSET r.i
       LIMIT 1) AS account_password
    FROM
      r
    ORDER BY proxy_address
  ) T;
