CREATE TABLE service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_code
  ON service (code);

CREATE TABLE service_account
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_service_account
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  service_id  INTEGER
    CONSTRAINT fk_service_account_service_id
    REFERENCES service (id),
  login       TEXT,
  password    TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_service_account_service_id_login
  ON service_account (service_id, login);

CREATE TABLE person
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_person
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_person_code
  ON person (code);

CREATE TABLE person_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_person_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  person_id          INTEGER
    CONSTRAINT fk_person_service_account_person_id
    REFERENCES person (id),
  service_account_id INTEGER
    CONSTRAINT fk_person_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE UNIQUE INDEX ux_person_service_account_service_account_id
  ON person_service_account (service_account_id);
CREATE INDEX ix_person_service_account_person_id_service_account_id
  ON person_service_account (person_id, service_account_id);

CREATE TABLE tag
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  code        TEXT,
  title       TEXT,
  description TEXT
);
CREATE UNIQUE INDEX ux_tag_code
  ON tag (code);

CREATE TABLE tag_service_account
(
  id                 SERIAL NOT NULL
    CONSTRAINT pk_tag_service_account
    PRIMARY KEY,
  insert_date        TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden          BOOLEAN                  DEFAULT FALSE,
  tag_id             INTEGER
    CONSTRAINT fk_tag_service_account_tag_id
    REFERENCES tag (id),
  service_account_id INTEGER
    CONSTRAINT fk_tag_service_account_service_account_id
    REFERENCES service_account (id)
);
CREATE INDEX ix_tag_service_account_tag_id_service_account_id
  ON tag_service_account (tag_id, service_account_id);

CREATE TABLE tag_service
(
  id          SERIAL NOT NULL
    CONSTRAINT pk_tag_service
    PRIMARY KEY,
  insert_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  is_hidden   BOOLEAN                  DEFAULT FALSE,
  tag_id      INTEGER
    CONSTRAINT fk_tag_service_tag_id
    REFERENCES tag (id),
  service_id  INTEGER
    CONSTRAINT fk_tag_service_service_id
    REFERENCES service (id)
);
CREATE UNIQUE INDEX ix_tag_service_tag_id_service_id
  ON tag_service (tag_id, service_id);
