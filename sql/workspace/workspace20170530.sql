-- used account total
SELECT *
FROM
  service_account sa
WHERE
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.id = sa.id
  );

-- unused proxy
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
  JOIN tag_service ts
    ON s.id = ts.service_id
  JOIN tag t
    ON ts.tag_id = t.id
WHERE
  t.code = 'PROXY'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

-- unused @mail.ru
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
WHERE
  s.code = 'MAIL_RU'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

-- unused pikabu.ru
SELECT *
FROM
  service_account sa
  JOIN service s
    ON sa.service_id = s.id
WHERE
  s.code = 'PIKABU_RU'
  AND
  NOT Exists
  (
      SELECT NULL
      FROM
        person_service_account psa
      WHERE
        psa.service_account_id = sa.id
  );

-- account total
SELECT *
FROM
  service_account sa
;

-- mail ru account
SELECT *
FROM
  service_account sa
join service s
  on sa.service_id = s.id
where
  s.code = 'MAIL_RU'
;

-- pikabu ru account
SELECT *
FROM
  service_account sa
join service s
  on sa.service_id = s.id
where
  s.code = 'PIKABU_RU'
;

SELECT *
FROM
  service_account sa
join service s
  on sa.service_id = s.id
  join tag_service ts
    on s.id = ts.service_id
  join tag t
    on ts.tag_id = t.id
where
  t.code = 'PROXY'
;

SELECT * from person;

SELECT *
FROM
  service_account sa
join service s
  on sa.service_id = s.id
  join tag_service_account tsa
    on sa.id = tsa.service_account_id
  join tag t
    on tsa.tag_id = t.id
where
  s.code = 'PIKABU_RU'
AND t.code = 'BLOCK'
;

SELECT *
FROM
  service_account sa
join service s
  on sa.service_id = s.id
  join tag_service_account tsa
    on sa.id = tsa.service_account_id
  join tag t
    on tsa.tag_id = t.id
where
  s.code = 'PIKABU_RU'
AND t.code = 'RELIABLE'
;