SELECT *
FROM service_account
WHERE service_id = (SELECT id
                    FROM service
                    WHERE code = 'YANDEX_MAIL');

DELETE FROM service_account
WHERE service_id = (SELECT id
                    FROM service
                    WHERE code = 'YANDEX_MAIL');

DELETE FROM service_account sa
WHERE
  service_id = (
    SELECT id
    FROM service
    WHERE code = 'MAIL_RU')
  AND login IN (
    'boryanikl3we@mail.ru',
    'olegzaloga6bc@mail.ru',
    'tolikdvo4nsd@mail.ru',
    'marinahzkmu@mail.ru',
    'waleriya87cbax@mail.ru',
    'kristinacm4e@mail.ru',
    'olga67dorin@mail.ru',
    'markustgx@mail.ru',
    'lilyawrxtok@mail.ru',
    'vladlenzpwoal@mail.ru',
    'lyubawaf1koz@mail.ru',
    'eduardsol27@mail.ru',
    'ewgeniyaosshpl@mail.ru',
    'leonidao4x@mail.ru',
    'borya3239s@mail.ru',
    'boryawc3@mail.ru',
    'ilya60eumed@mail.ru',
    'lyubowex5b@mail.ru',
    'natalyas5bn@mail.ru',
    'innacz8kfilov@mail.ru',
    'daryafas8d@mail.ru',
    'yaroslavaiwh@mail.ru',
    'leonardi8kdo@mail.ru',
    'vladislavvvlnm@mail.ru',
    'polinazwvs@mail.ru',
    'ulyanamasa@mail.ru',
    'gersychovnul3@mail.ru',
    'nadezhdamqse@mail.ru'

  );